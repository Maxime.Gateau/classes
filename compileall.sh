#!/bin/bash

echo Saving algebra...
bash compilealgebra.sh

echo Saving analysis...
bash compileanalysis.sh

echo Saving discrete mathematics...
bash compilediscrete.sh

echo Saving geometry...
bash compilegeo.sh

echo Saving systems programming...
bash compilesys.sh

git add *
read -p "Enter a commit message: " commit_msg
git commit -m "$commit_msg"
git push