# TODO - Semester

- small webapp that catalogs latex colors. allow user to select color, returns package and command for closest matching color in database (sort by popularity)
- script to replace all instances of \Longrightarrow by \implies and \Longleftrightarrow by \iff
- build file for future classes

- from now on, latex is for LECTURE NOTES ONLY. all exercises for non-cs courses will be done on tablet and imported to at the end of lecture notes of relevant course.

- [x] sign up for exams
- [ ] apply for job ARA
- [ ] finish student visa
- [ ] become legal resident of geneva


