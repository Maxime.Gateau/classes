\setchapterstyle{kao}
\chapter{Principe d'inclusion/exclusion}
\labch{inc}

Le principe d'addition \( E_1 \sqcup \cdots \sqcup E_n \) nous donne le cardinal de l'union disjointe :
\( |E_1| + \ldots + |E_n| \). Que se passe-t-il si les ensembles $E_i$ ne sont pas disjoints ?

\section{Cas particuliers simples}

\begin{theorem}[Principe de soustraction]
    Soient $E$ et $F$ deux ensembles finis tels que $F \subset E$. On a $|E \setminus F| = |E| - |F|.$
    \marginnote{On rappelle que $E \setminus F \coloneqq \{ x \in E : x \notin F \}$}
\end{theorem}

\begin{proof}
    \[ E = F \sqcup (E \setminus F) \text{ et par le principe d'addition } |E| = |F| + |E \setminus F|. \]
\end{proof}

Compter les mots de 10 lettres sur l'alphabet français usuel qui ont au moins une lettre qui se répète.
\begin{itemize}
    \item On compte d'abord l'ensemble $E$ des mots de 10 lettres sur $\{ a,b,c, \ldots, z\}$ : il y en a $|E| = 26^{10}$,
    soit 26 choix pour chaque lettre.
    \item On enlève le cardinal de l'ensemble $F$ des mots de 10 lettres distinctes sur $\{ a, \ldots, z \}$ : 
    $|F| = A(26,10) = \frac{26!}{16!}.$
    \item Par le principe de soustraction, l'ensemble $E \setminus F$ des mots de 10 lettres sur $\{ a, \ldots, z \}$ où au
    moins une lettre se répète a pour cardinal $|E \setminus F| = |E| - |F| = 26^{10} - A(26,10).$
\end{itemize}

Le principe de soustraction est le cas particulier le plus simple du principe d'inclusion-exclusion. Voyons un autre cas particulier.

\begin{proposition}
    Soit $E$ un ensemble fini, $F_1$ et $F_2$ deux sous-ensembles de $E$. Notons \( \overline{F_1} = E \setminus F_1 \text{ et } \overline{F_2} = E \setminus F_2 \).
    Alors, \( |\overline{F_1} \cap \overline{F_2}| = |E| - |F_1| - |F_2| + |F_1 \cup F_2|. \)
\end{proposition}

\begin{proof}
    $\overline{F_1} \cap \overline{F_2}$ est l'ensemble des éléments de $E$ qui ne sont ni dans $F_1$ ni dans $F_2$. On commence
    par compter tous les éléments de $E$, puis on enlève 1 à ce nombre pour chaque élément de $F_1$ et chaque élément de $F_2$,
    ce qui nous donne $|E| - |F_1| - |F_2|$. Mais on a retiré deux fois les éléments de $F_1 \cap F_2$, donc on rajoute $|F_1 \cap F_2|$
    pour compenser. 
\end{proof}

\begin{proof}
    On compte, de chaque côté de l'égalité $|\overline{F_1} \cap \overline{F_2}| = |E| - |F_1| - |F_2| = |F_1 \cap F_2|$, combien
    de fois est compté chaque élément de $E$.
    \begin{align*}
        \text{À gauche, } \text{ si } x \in \overline{F_1} \cap \overline{F_2}, \text{ il est compté 1 fois et } \\
        \text{ si } x \in E \setminus (\overline{F_1} \cap \overline{F_2}), \text{ il est compté 0 fois.} \\
        \\
        \text{À droite, } \text{ si } x \in \overline{F_1} \cap \overline{F_2}, \text{ alors } \\
        x \in E \\
        x \notin F_1 \\
        x \notin F_2 \\
        x \notin F_1 \cap F_2. \\
    \end{align*}
    Il est donc compté $1-0-0+0 = 1$ fois.
    %TODO finish the proof!
\end{proof}

\begin{corollary}
    \[  |F_1 \cup F_2| = |F_1| + |F_2| - |F_1 \cap F_2|. \]
\end{corollary}

\section{Généralisation}

On a vu les cas avec un ou deux sous-ensembles de $E$. Voyons le cas général avec $m$ sous-ensembles.

\begin{theorem}[Principe d'inclusion-exclusion généralisé]
    Soient $E$ un ensemble fini, $F_1, \ldots, F_m$ des sous-ensembles de $E$. On note 
    $\overline{F_k} = E \setminus F_k, \forall k \in \llbracket 1, m \rrbracket$. Alors,
    \begin{equation} \label{eq1}
        \overline{F_1} \cap \ldots \cap \overline{F_m}| = |E| + 
        \sum_{k=1}^{m} (-1)^k \cdot \sum_{\substack{ J \subset \llbracket 1, m \rrbracket \\ 
        |J| = k}} \left| \bigcap_{j \in J} F_j \right|.
    \end{equation}
\end{theorem}
\begin{proof}
    Comme pour le cas $m = 2$ vu la dernière fois, on veut montrer que chaque élément de $E$ est compté le même nombre de fois
    à gauche et à droite de l'égalité.
    \begin{itemize}
        \item Si \( x \in \overline{F_1} \cap \ldots \cap \overline{F_m} = E \setminus (F_1 \cup \ldots \cup F_m), \) alors 
        $x$ est compté une fois à gauche de \ref{eq1} et une fois à droite (il appartient à $E$ mais à aucun des $F_k$ donc à
        aucun des $\bigcap_{j \in J} F_j$)
        \item Si $x$ appartient à $n$ des sous-ensembles $F_1, \ldots, F_m$ pour $n \in \llbracket 1, m \rrbracket,$ alors $x$
        est compté $0$ fois à gauche de \ref{eq1}. À droite de \ref{eq1}, x est compté $\binom{n}{0} = 1$ fois dans E.
    \end{itemize}
    
    D'où $x$ est compté 
    \begin{itemize}
        \item $n = \binom{n}{1}$ fois dans \[ \sum_{j=1}^{m}\abs{F_j} \] lorsque $k=1$ et 
        \item $n = \binom{n}{2}$ fois dans \[ \sum_{\substack{ J \subset \llbracket 1, m \rrbracket \\ |J| = k}} \left| \bigcap_{j \in J} F_j \right| \] lorsque $k=2.$
        \item $n = \binom{n}{k}$ fois dans \[ \sum_{\substack{ J \subset \llbracket 1, m \rrbracket \\ |J| = k}} \left| \bigcap_{j \in J} F_j \right| \]
    \end{itemize}

    En effet, pour que x appartiennent à $F_i \cap F_j,$ il faut que $i$ et $j$ appartiennent à l'ensemble des $n$ indices
    $\{ i_1, \ldots, i_n \}$ tel que $x \in F_1 \cap \ldots \cap F_m.$ \\

    Enfin, là encore pour que $x$ soit dans $\bigcap_{j \in J} F_j,$ il faut que $J$ (qui à $k$ éléments), soit un sous-ensemble
    de $\{i_1, \ldots, i_n\}.$ Il y a donc $\binom{n}{k}$ sous-ensembles $J$ de $\llbracket 1, m \rrbracket$ tel que 
    $x \in \bigcap_{j \in J}F_j.$ \\
    
    Ainsi, au total, $x$ apparaît \[ \sum_{k=1}^{m} (-1)^k \cdot \sum_{\substack{ J \subset \llbracket 1, m \rrbracket \\ 
    |J| = k}} \left| \bigcap_{j \in J} F_j \right| = 0 \text{ fois.} \] 
\end{proof}

\section{Exemples d'applicaiton}

\subsection{Divisibilité}

\indent Combien y a-t-il d'entiers compris entre 1 et 600 qui ne sont divisibles ni par 4, ni par 5, ni par 6 ?
Posons \[ E  = \{ \text{ entiers entre 1 et 100} \} \text{ et } A_k = \{ \text{ ensemble des entiers entre 1 et 600 qui sont divisibles par }k \} \] 
Le résultat recherché est \[ \abs{E \setminus (A_4 \cup A_5 \cup A_6)} = \abs{\overline{A_4} \cap \overline{A_5} \cap \overline{A_6}}. \]
Calculons-le par le principe d'inclusion-exclusion. 
\begin{align*}
    \card{\bar{A_4} \cap \bar{A_5} \cap \bar{A_6}} &= \card{E} - (\card{A_4} + \card{A_5} + \card{A_6}) \\
    &+ \\
    &- \\

\end{align*}

\indent D'où $A_4 \cap A_5$ est l'ensemble des entiers entre 1 et 600 qui sont divisibles par 4 et 5, donc par ppcm$(4,5) = 20.$ 
Donc $A_4 \cap A_5 = A_20$ et \[ \card{A_4 \cap A_5} = \card{A_20} = \frac{600}{20} = 30. \] 

\indent De même, $A_5 \cap A_6 = A_30,$ car ppcm$(5,6) = 30$ et donc \[ \card{A_5 \cap A_6} = \card{A_30} = \frac{600}{30} = 20 \]

\indent Enfin 


\subsection{Dérangements}

\begin{definition}[Dérangement]
    Un \emph{dérangement} de $\llbracket 1, n \rrbracket$ est une permutation \[ i_1 \cdots i_n \text{ où } i_k \neq k \quad \forall k, \]
    c'est-à-dire une \emph{permutation sans point fixe}. On note $D_n$ le nombre de dérangements.
    \marginnote{$2143$ est un dérangement de $\{1,2,3,4\}$ \\ $3241$ n'est pas un dérangement, car $2$ est un point fixe.}
\end{definition}

\begin{theorem}[Formule du dérangement]
    \[ \forall n \geq 1, \quad D_n = n! \cdot \sum_{k=0}^{n} \frac{(-1)^k}{k!} \]
\end{theorem}
\begin{proof}
    Soit $E$ l'ensemble des permutations de $\llbracket 1, n \rrbracket.$ Pour tout $k \in \llbracket 1, n \rrbracket,$ 
    soit $F_k$ l'ensemble des permutations ayant $k$ comme point fixe. On note $\bar{F_k} = E \setminus F_k.$ Alors, l'ensemble
    des dérangements de $\llbracket 1, n \rrbracket$ est $\bar{F_1} \cap \ldots \cap \bar{F_n}.$ D'après le principe d'inclusion-exclusion,
    \begin{align*}
        D_n &= | \bar{F_1} \cap \ldots \cap \bar{F_n} \\
        &= \card{E} + \sum_{k=1}^{n} (-1)^k \cdot \sum_{\substack{ J \subset \llbracket 1, m \rrbracket \\ 
        |J| = k}} \left| \bigcap_{j \in J} F_j \right|.
    \end{align*}
    Si $J$ est un sous-ensemble de $\llbracket 1,n \rrbracket$ à $k$ éléments, alors \( \bigcap_{j \in J}F_j = (n-k)!, \)
    car les $k$ entiers appartient à $J$ sont des points fixes, et on a donc $(n-k)!$ manières d'ordonner les $n-k$ autres
    éléments de $\llbracket 1,n \rrbracket.$

    Enfin, il y a $\binom{n}{k}$ sous -ensembles $J$ de $\llbracket1,n\rrbracket$ à $k$ éléments, donc 
    \begin{align*}
        \sum_{\substack{ J \subset \llbracket 1, m \rrbracket \\ |J| = k}} \left| \bigcap_{j \in J} F_j \right| &= \binom{n}{k}(n-k)! \\
        &= \frac{n!}{k!}
    \end{align*}

    Pour finir,
    \begin{align*}
        D_n &= \sum_{k=1}^{m} (-1)^k \frac{n!}{k!} \\
        &= n! \sum_{k=0}^{n} \frac{(-1)^k}{k!}
    \end{align*}
\end{proof}