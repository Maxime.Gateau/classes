\setchapterstyle{kao}
\chapter{Séries génératrices}
\labch{sgen}


\indent En combinatoire, on veut souvent calculer le cardinal d'une suite d'ensembles $(E_n)_{n \in \mathbb{N}}.$ Il y a
plusieurs manières de le faire (nous avons déjà vu les deux premières).
\begin{enumerate}
    \item Trouver une formule exacte directement. \sidenote{$E_n = \{ \text{permutations de } \llbracket1,n\rrbracket \},$ \\ où $\card{E_n} = n!.$}
    \item Trouver une relation de récurrence. \sidenote{$E_{k,n} = \{ \text{\small parties à k éléments de } \llbracket1,n\rrbracket \},$ \\ où $E_{k,n} = \binom{n}{k}$}
    \item Calculer la série génératrice. \sidenote{Si $(E_n)_{n \in \mathbb{N}}$ est une suite d'ensembles finis, la série génératrice de $(E_n)_{n \in \mathbb{N}}$ est \[ \sum_{n \geq 0} \card{E_n}X^n \]}
\end{enumerate}

On va montrer dans ce chapitre que la série génératrice de $(P_n)_{n \in \mathbb{N}}$ où $P_n$ est l'ensemble des partitions
de l'entier $n,$ est \[ \sum_{n \in \mathbb{N}} p(n)X^n = \prod_{k \geq 1} \frac{1}{1-x^k} \]

La série génératrice de $(E_n)_{n \in \mathbb{N}}$ contient l'information de tous les $\card{E_n}$ à la fois.


\section{L'anneau des séries formelles}

Les séries formelles sont des généralisations des polynômes avec une infinité de termes. Elles permettent de considérer les séries qu'on étudie en analyse \emph{indépendemment de la notion de convergence}. C'est là toute la différence entre série \emph{formelle} et série \emph{analytique}. Une série formelle est un objet purement algébrique, \cad qu'une série formelle ne représente pas forcément une fonction qui \guillemetleft existe \guillemetright  à proprement parler. En réalité, on s'en fiche un peu qu'une série formelle \guillemetleft existe \guillemetright; le but de l'anneau des séries formelles est d'établir les règles de manipulation de celles-ci dans la construction d'un \guillemetleft calculus \guillemetright  de séries génératrice. N'oublions pas, au fil de ce chapitre, que notre but est de pouvoir mieux énumérer les objets mathématiques.

\begin{definition}[Série formelle]
    Soit $\mathbb{A}$ un anneau commutatif ou un corps. Une \emph{série formelle} sur $\mathbb{A}$ de variable $X$ est une expression
    de la forme \[ \sum_{n \geq 0} a_n X^n \] où $(a_n)_{n \in \mathbb{N}}$ est une suite d'éléments de $\mathbb{A}$ appelée
    \emph{suite de coefficients} de la série formelle. On note $\mathbb{A}\llbracket X \rrbracket$ l'ensemble de séries formelles sur $\mathbb{A}$
    en la variable X.
\end{definition}

\marginnote{Si $A(X) = \sum_{n \geq 0} a_nX^n,$ on note $[X^n]A(X) = a_n,$ le coefficient de $X^n$ dans la série formelle $A(X).$} 

\begin{definition}[Égalité]
    Deux séries formelles $\sf{a}$ et $\sf{b}$ sont égales \ssi  \( a_n = b_n, \quad \forall n \geq 0. \)
\end{definition}

\begin{proposition}
    \( (\mathbb{A}, +, \times) \) est un anneau commutatif où $n \in \N$ avec
    \begin{align*}
        \text{Élément neutre pour }+\text{ : } \quad &\mathbb{0} = \sum_{n \geq 0} 0_{\mathbb{A}} X^n \\
        \text{Élément neutre pour }\times \text{ : } \quad &\mathbb{1} = \mathbb{1}_{\mathbb{A}} + \sum_{n \geq 1} \Zero_\A X^n \\
        + \text{ défini par : } \quad &\left(\gen{a}\right) + \left( \gen{b} \right) = \sum_{n \geq 0} (a_n + b_n) X^n \\
        \times \text{ défini par : } \quad &\left( \gen{a} \right) \times \left( \gen{b} \right) = \gen{c} \\
        &\text{avec } c_n = \sum_{k = 0}^{n} a_k b_{n-k}, \quad \forall n \geq 0.
    \end{align*}
\end{proposition}
\begin{proof}
    
\end{proof}


Prenons un exemple. $1-X$ est l'inverse de $\sum_{n \geq 0} X^n.$ En effet, 
\begin{align*}
    X \cdot \sum_{n \geq 0} X^n &= \sum_{n \geq 0} X^{n+1}. \text{ D'où,} \\
    (1 - X) \cdot \sum_{n \geq 0} X^n &= \sum_{n \geq 0} X^n - \sum_{n \geq 0} X^{n+1} \\
    &= \One.
\end{align*}
On retrouve de manière formelle la célèbre formule pour la somme d'une série géométrique : \[ \sum_{n \geq 0} X^n = \frac{1}{1-X}. \]


\begin{definition}[Dérivée formelle]
    La \emph{dérivée formelle} d'une série formelle $A(X) = \gen{a}$ est la série formelle \[ A^{\prime}(X) = \frac{d}{d x} A(X) \coloneqq \sum_{n \geq 1} n a_n X^{n-1}. \]
\end{definition}

Considérons la fonction exponentielle : \[ e^X = \sum_{n \geq 0} \frac{X^n}{n!}. \] Alors,
\begin{align*}
    \frac{d}{d x} e^X &= \sum_{n \geq 1} \frac{nX^{n-1}}{n!} \\
    &= \sum_{n \geq 1} \frac{X^{n-1}}{(n-1)!} \\
    &= \sum_{n \geq 0} \frac{X^n}{n} = e^X.
\end{align*}


\begin{definition}[Substitution]
    Soient $A(X) = \gen{a}$ et $B(X) = \gen{b}$ des séries formelles dans $\A \llbracket X \rrbracket,$ avec $b_0 = \Zero_\A.$
    La \emph{substitution de $B$ dans $A$} est définie par \[ A(B(X)) = \sum_{n \geq 0} a_n \left( B(X) \right)^n. \] 
\end{definition}

\begin{proposition}
    \begin{flalign*}
        &A\left(B(X)\right) = \gen{\alpha} \text{ avec } \\
        &\begin{cases}
            \alpha_0 = a_0 \\
            \alpha_n = \sum_{k=1}^{n} \alpha_k \beta_{n,k}, \quad \forall n \geq 1, \text{ où } \beta_{n,k} = [X^n]\left(B(X)\right)^k
        \end{cases}
    \end{flalign*}
\end{proposition}
\begin{proof}
    On développe $(B(X))^n$ dans la définition.
\end{proof}

En effet, si la série $B(X)$ contient un terme non nul, alors il se pourrait que les termes de $A(B(X))$ contribuent aux coefficients des puissances de $x.$ En revanche, si $b_0 = 0,$ on peut calculer le coefficient de $X^n$ à partir des $n-1$ premiers termes de la série génératrice. Ainsi, le fait que le terme initial $b_0$ soit nul, le calcul d'un coefficient quelconque de $A(B(X))$ est un processus \emph{fini} et par conséquent, $A(B(X))$ est bien définie (puisque les coefficients sont bien-définis). En revanche, si $b \neq 0,$ le calcul de chaque coefficient est un processus \emph{infini}, à moins que $A(X)$ soit un polynôme. De ce fait, l'étude de la série $A(B(X))$ ne fait sens que si elle converge. Mais, comme on l'a dit dans l'introduction de ce chapitre, dans l'étude de l'anneau de séries formelles, la notion de convergence n'existe pas ! Donc, en pratique, la composition $A(B(X))$ de deux séries formelles est bien définie \ssi  $b_0 = 0$ \emph{ou} $A(X)$ est un polynôme (\cad  $a_n = \Zero_\A$ à partir d'un certain rang).

Prenons un exemple. Soient $A(X) = \frac{1}{1-X}$ et $B(X) = X + X^2.$ On obtient 
\begin{align*}
    A(B(X)) = \frac{1}{1-(X+X^2)} &= \sum_n (X+X^2)^n \\
    &= \sum_n \sum_{k=0}^{n} \binom{n}{k} X^{2n-k}.
\end{align*}

\section{Séries formelles à plusieurs variables}

Soit $\A$ un anneau commutatif. On définit \[ \A \llbracket X,Y \rrbracket = \left\{ \sum_{n,k \geq 0} a_{n,k} X^n Y^k : a_{n,k} \in \A \forall n,k \in \N \right\} \] l'ensemble des séries formelles en deux variables $X$ et $Y$ sur $\A.$

En remarquant que $\A \llbracket X,Y \rrbracket = \left( \A \llbracket X \rrbracket \right) \llbracket Y \rrbracket,$ comme on a montré que $\ringA{X}$ est un anneau commutatif, alors $\ringA{X,Y}$ possède aussi une structure d'anneau commutatif. 

On peut généraliser par récurrence cette notion à variables en définissant $\ringA{X_1, \ldots , X_n} = \left( \ringA{X_1, \ldots , X_{n-1}} \right) \llbracket X_n \rrbracket.$

\section{Les séries génératrices en combinatoire}

Soit $\seq{a}$ une suite d'éléments d'un anneau $\A.$ Alors la série formelle $\gen{a}$ est appelée la \emph{série génératrice ordinaire} de $\seq{a}.$

\begin{definition}
    La \emph{série génératrice exponentielle} de $\seq{a}$ est la série formelle \[ \sum_n \frac{a_n X^n}{n!}. \]
\end{definition}

La série génératrice ordinaire de $(1, 1, \ldots)$ est $\sum_n X^n = \frac{1}{1-X}$ et sa série génératrice exponentielle est $\sum_n \frac{X^n}{n!} = e^X.$ \marginnote{Lorsqu'on ne précise rien, on parle de séries génératrices ordinaires.}

\begin{proposition}
    Soient \[ A(X) = \sum_n \frac{a_n X^n}{n!} \] la série génératrice exponentielle de $\seq{a}$ et soit  \[ B(X) = \expgen{b} \] la série génératrice exponentielle de $\seq{b}.$ Alors, 
    \begin{enumerate}
        \item $A(X) + B(X)$ est la \sge de $(a_n + b_n)_{n \in \N}.$
        \item $A(X) \cdot B(X)$ est la \sge de \[ \left( \sum_{k=0}^{n} \binom{n}{k} a_k b_{n-k} \right)_{n \in \N}. \]
    \end{enumerate} 
\end{proposition}
\begin{proof}
    En utilisant les règles d'addition et de multiplication dans $\ringA{X}.$
\end{proof}

On va maintenant définir les séries génératrices d'ensembles d'objets combinatoires.

\begin{definition}[Classe combinatoire]
    Une \emph{classe combinatoire} $\cc$ est un ensemble fini ou dénombrable muni d'une fonction de taille $t : \cc \longrightarrow \N.$ On note \[ \cc_n = \{ c \in \cc : t(c) = n \} \] et $c_n = \card{\cc_n}.$
\end{definition}

Pour illustrer cette définition, prenons $\cc = \left\{ \text{ mots sur l'alphabet } \{ 0,1 \} \right\}.$ Alors,
$t(c) = \text{ longueur du mot } c, \forall c \in \cc.$ On a $\cc_n = \left\{ \text{ mots de longueur } n \text{ sur } \{ 0,1 \} \right\}.$ et $c_n = \card{\cc_n} = 2^n.$

Considérons un autre ensemble familier. Soit $\cc = \{ \text{ partitions d'entiers } \}.$ Alors, $t(c) = \text{ somme des parts de } c.$ D'où, $\cc = \{ \text{ partitions de n } \}.$ Donc, $c_n = \card{\cc_n} = \mathscr{p}(n).$

\begin{definition}
    Soit $\cc$ une classe combinatoire avec fonction de taille $t.$ La série génératrice de $\cc$ est la série formelle 
    \begin{align*}
        C(X) = \gen{c}, \quad \text{ où } c_n &= \card{\cc_n} \\
        &= \card{\{ c \in \cc : t(c) = n \}}.
    \end{align*}
    \marginnote{On peut aussi écrire, de manière équivalente, \[ C(X) = \sum_{c \in \cc} X^{t(c)}. \]}
\end{definition}

Reprenons le premier exemple : $\cc = \left\{ \text{ mots sur l'alphabet } \{ 0,1 \} \right\}.$ Armés de la définition ci-dessus, nous pouvons écrire \[ C(X) = \gen{c} = \sum_n 2^n X^n = \frac{1}{1-2X}. \]

Nous venons de voir qu'il est possible d'exprimer la cardinalité d'un ensemble comme le coefficient d'une série génératrice bien choisie. Autrement dit, les coefficients d'une série formelle ont une interprétation combinatoire. Remarquons dorénavant que les opérations d'addition et de multiplication sur les séries formelles ont elles aussi une interprétation combinatoire.

\begin{definition}[Union disjointe]
    Soient $\cc$ et $\dd$ deux classes combinatoires, avec fonctions de tailles respectives $t_{\cc}$ et $t_{\dd},$ telles que $\cc \cap \dd = \emptyset.$ L'\emph{union disjointe} de $\cc$ et $\dd,$ notée $\cc \sqcup \dd,$ est la classe combinatoire $\cc \sqcup \dd$ avec une fonction de taille 
    \begin{align*}
        t : \cc \sqcup \dd &\longrightarrow \N \\
        x &\mapsto \begin{cases*}
            t_{\cc}(x) \quad \text{ si } x \in \cc \\
            t_{\dd}(x) \quad \text{ si } x \in \dd
        \end{cases*}
    \end{align*}
\end{definition}

\begin{proposition}
    Si $C(X)$ est la série génératrice de $\cc$ et $D(X)$ la série génératrice de $\dd,$ alors, $C(X) + D(X)$ est la série génératrice de $\cc \sqcup \dd.$ 
\end{proposition}
\begin{proof}
    On utilise la définition de série génératrice ordinaire.
\end{proof}

Soient $\cc = \{ \text{ mots sur } \{0,1\} \}$ et $\dd = \{ \text{ mots sur } \{ a,b,c \} \}.$ On a, pour tout n, $c_n = 2^n$ et $d_n = 3^n.$ Alors, \[ \cc \sqcup \dd = \text{ mots avec seulement des lettres soit dans } \{ 0,1 \} \text{ soit dans } \{ a,b,c \}. \] D'où, $a_n = \card{(\cc \sqcup \dd)_n} = 2^n + 3^n.$ Donc, la série génératrice de $\cc \sqcup \dd$ est \[ C(X) + D(X) = \frac{1}{1-2X} + \frac{1}{1-3X} = \sum_n (2^n + 3^n) X^n. \]

\begin{definition}[Produit cartésien]
    Soient $\cc$ et $\dd$ deux classes combinatoires avec fonctions de tailles respectives $t_{\cc}$ et $t_{\dd}.$ Le produit cartésien de $\cc$ et $\dd$ est la classe combinatoire \[ \cc \times \dd = \left\{ (c,d) : c \in \cc, d \in \dd \right\} \] dont la fonction de taille est donnée par 
    \begin{align*}
        t : \cc \times \dd &\longrightarrow \N \\
        (c,d) &\mapsto t_{\cc}(c) + t_{\dd}(d).
    \end{align*}
\end{definition}

\begin{proposition}
    Si $C(X)$ est la série génératrice de $\cc$ et $D(X)$ est la série génératrice de $\dd,$ alors $C(X) \cdot D(X)$ est la série génératrice de $\cc \times \dd.$
\end{proposition}
\begin{proof}
    Les éléments de taille $n$ dans $\cc \times \dd$ sont exactement les couples $(c,d)$ tels que $t_{\cc}(c) = k$ et $t_{\dd}(d) = n-k$ pour un certain $k \in \llbracket 0,n \rrbracket.$
\end{proof}

Reprenons l'exemple précédent. 
\begin{align*}
    \cc = \{ \text{ mots sur } \{0,1\} \} \text{ et } \dd = \{ \text{ mots sur } \{ a,b,c \} \}. \\
    \cc \times \dd = \{ (u,v) : u \text{ mot sur } \{0,1\}, v \text{ mot sur } \{a,b,c\} \} \\
    \text{La série génératrice de } \cc \times \dd : \quad C(X) \cdot D(X) = \frac{1}{1-2X} \cdot \frac{1}{1-3X}.
\end{align*}

Il est important de remarquer que pour montrer que $\forall n \geq 0, a_n = b_n,$ il suffit de montrer que leurs séries génératrices (ordinaire ou exponentielles) sont égales. En effet, si $\gen{a} = \gen{b},$ alors en extrayant le coefficient de $X^n$ de chaque côté pour tout $n,$ on retrouve $a_n = b_n$ pour tout $n.$

Pour montrer que pour tout $n$ deux ensembles d'objets combinatoires de taille $n$ ont le même cardinal, il suffit donc aussi de montrer que leurs séries génératrices sont égales.

Par exemple, considérons les deux ensembles suivants. 
\begin{align*}
    \cc_{n,k} = \{ \text{ combinaisons de k éléments parmi n } \} \\
    \dd_{n,k} = \{ \text{ mots de n lettres sur } \{a,b\} \text{ avec k fois la lettre \guillemetleft a \guillemetright  }\}.
\end{align*}
On sait d'ores et déjà que $c_{n,k} = \card{\cc_{n,k}} = \binom{n}{k}.$ Mais qu'en est-il de $d_{n,k} = \card{\dd_{n,k}} ?$ On trouve alors, \[ C_n(X) \coloneqq \sum_k \binom{n}{k} X^k = (1+X)^n \] par le binôme de Newton. Or, \[ \dd_{n,k} = \{ (\l_1, \ldots, \L_n) : \l_i \in \{a,b\} \forall i \in \llbracket 1,n \rrbracket \text{ avec k des } \l_i \text{ égaux à \guillemetleft a \guillemetright}\}. \] D'où la série génératrice pour une lettre $\l_i$ est $(1+X)$ avec le membre de gauche (de l'opérateur $+$) représentant les cas $\l_i = b$ et où le membre de droite indique les cas où $\l_i = a.$ Donc, par la règle de multiplication des séries génératrices, 
\begin{align*}
    D_n(X) &\coloneqq \sum_k d_{n,k} X^k \\
    &= (1+X) \cdot \ldots \cdot (1+X) \\
    &= (1+X)^n.
\end{align*}
\marginnote{Pour un autre exemple, voir l'identité d'Euler sur les partitions d'entiers (à la fin du chapitre).}
On obtient $C_n(X) = D_n(X)$ et ainsi, $d_{n,k} = c_{n,k} = \binom{n}{k}.$

\section{Calculs de séries génératrices classiques}

\subsection*{Nombres de Fibonacci}

Rappelons que la suite des nombres de Fibonacci $\seq{F}$ est définie par $F_0 = 0, F_1 = 1$ et
\begin{equation} 
    \forall n \geq 2, F_n = F_{n-1} + F_{n-2}. \label{eq:1}
\end{equation}
$F_n$ est le nombre de sous-ensembles de $\llbracket 1, n-2 \rrbracket$ ne contenant pas deux entiers consécutifs (pour $n  \geq 3).$ Multiplions \eqref{eq:1} par $X^n$ et sommons sur $n \geq 2 :$
\begin{align*}
    \sum_{n \geq 2} F_n X^n &= \sum_{n \geq 2} F_{n-1} X^n + \sum_{n \geq 2} F_{n-2} X^n \\
    &= X \sum_{n \geq 1} F_n X^n + X^2 \sum_{n \geq 0} F_n X^n
\end{align*}
En notant $F(X) = sum_{n \geq 0} F_n X^n,$ on a donc
\begin{align*}
    F(X) - F_1 X - F_0 &= X (F(X) - F_0) + X^2 F(X) \\
    (1 - X - X^2) F(X) &= X \\
    \Longrightarrow F(X) = \frac{X}{1-X-X^2}.
\end{align*}


\subsection*{Nombres de Catalan}

Les nombres de Catalan $C(n)_{n \geq 0}$ apparaissent dans de nombreux problèmes en combinatoire et comptent plusieurs types d'objets :
\begin{itemize}
    \item $C_n$ est le nombre de chemins de Dyck de longueur $z_n$ commençants en $(0,0)$ et finissant en $(z_n,0)$ ne passant jamais sous l'axe des abscisses.
    \item $C_n$ est le nombre de manières de placer correctement $z_n$ parenthèses.
    \item $C_n$ est le nombre de manières de découper en triangles un polygône convexe à $n+2$ côtés, avec des segments ayant pour extrémités des sommmets du
    polygône et ne s'intersectant pas.
\end{itemize} 

\begin{proposition}
    $C_0 = 1$ et \[ \forall n \geq 0, \quad C_{n+1} = \sum_{k = 0}^{n} C_k C_{n-k} \]
\end{proposition}

\begin{proof}
    Tout chemin de Dyck de longueur $2(n+1)$ peut être décomposé de manière unique sous cette forme. (drawing)
    Par les principes d'addition et de multiplication, on obtient $C_{n+1} = \sum_{k = 0}^{n} C_k C_{n-k}.$
\end{proof}

\begin{proposition}
    Soit $C(X) = \sum_{n \geq 0} C_n X^n.$ Alors, $C(X) = 1+XC^2(X).$ 
\end{proposition}
\begin{proof}
    D'après la règle de multiplication des séries formelles, $C^2(X)$ est la série génératrice de $\left(\sum_{k = 0}^{n} C_k C_{n-k}\right)_{n \geq 0}$ (ref la relation de la proposition ci-dessus).
    D'après la proposition précédente, 
    \begin{align*}
        \sum_{n \geq 0} C_{n+1} X^n &= \sum_{n \geq 0} \left( \sum_{k = 0}^{n} C_k C_{n-k} \right) X^n = C^2(X). \\
        \text{Donc } \sum_{n \geq 0} C_{n+1} X^{n+1} &= C(X) - C_0 = C(X) - 1 = XC^2(X)
    \end{align*}
\end{proof}

\begin{proposition}
    $\forall n \geq 0, \quad c_n = \frac{1}{n+1} \binom{2n}{n}.$
\end{proposition}
\begin{proof}
    On résout l'équation $XC^2(X) - C(X) + 1 = 0.$ Il y a deux solutions possibles : 
    \begin{align*}
        \frac{1-\sqrt{1-4X}}{2X} \text{ ou } \frac{1+\sqrt{1-4X}}{2X}
    \end{align*}
    On sait que $C_0 = 1.$ Or, la deuxième solution tend vers l'infini quand $X \longrightarrow 0,$ donc ça ne peut pas être la bonne. 
    En revanche, la première solution tend bien vers 1 :
    \begin{align*}
        \frac{(1-\sqrt{1-4X})(1+\sqrt{1-4X})}{2X(1+\sqrt{1-4X})} &= \frac{1-(1-4X)}{2X(1+\sqrt{1-4X})} \\
        &= \frac{2}{X(1+\sqrt{1-4X})} \longrightarrow_{x_\rightarrow 0} 1
    \end{align*}
    Donc $C(X) = \frac{1-\sqrt{1-4X}}{2X}.$

    On utilise la formule du binôme généralisé :
    \begin{align*}
        \forall \alpha \in \R, \quad (1+X)^\alpha &= \sum_{n \geq 0} \binom{\alpha}{n} X^n \\
        \text{où } \binom{\alpha}{n} &= \frac{\alpha(\alpha - 1) \cdots (\alpha - n + 1)}{n!}. \\
        \text{On trouve } \sqrt{1-4X} &= \sum_{n \geq 0} \binom{\sfrac{1}{2}}{n} (-4X)^n \\
        &= \sum_{n \geq 0} \frac{ \frac{1}{2} \cdot \left(\frac{-1}{2}\right) \cdot \left(\frac{-3}{2}\right) \cdots \left(\frac{-3}{2} - n\right)}{n!} \cdot (-4)^n X^n\\
        &= 1 - \sum_{n \geq 1} \frac{1 \cdot 3 \cdots (2n-3)}{n!} \cdot 2^n \cdot X^n \\
        &= 1 - \sum_{n \geq 1} \frac{2n!}{n! 2^n n!} \cdot \frac{2^nX^n}{2n-1} = 1 - \sum_{n \geq 1} \binom{2n}{n} \frac{X^n}{2n-1}.  \\
        \text{Ainsi } C(X) = \frac{1 - \sqrt{1 - 4X}}{2X} &= \sum_{n \geq 1} \frac{\binom{2n}{n}X^{n-1}}{2(2n-1)} \\
        &= \sum_{n \geq 0} \frac{\binom{2n+2}{n+1}X^n}{2(2n+1)}. \\
        \text{Donc } C_n = \frac{\binom{2n+2}{2n+1}}{2(2n+1)} &= \frac{(2n+2)!}{2(2n+1)(n+1)!(n+1)!} \\
        &= \frac{(2n+2)(2n+1)(2n)!}{(2n+1)2(n+1)(n+1)n!n!} \\
        &= \frac{\binom{2n}{n}}{n+1}.
    \end{align*}
\end{proof}

\subsection{Partitions d'entiers}

\begin{proposition}
    Soit $\mathcal{p}^{(k)}(n)$ le nombre de partitions de $n$ en parts toutes égales à $k.$ Alors \[ \sum_{n \geq 0} \mathcal{p}^{(k)}(n) X^n = \frac{1}{1-X^k}. \]
\end{proposition}
\begin{proof}
    
\end{proof}

