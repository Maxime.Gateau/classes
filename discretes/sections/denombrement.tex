\setchapterstyle{kao}
\chapter{Dénombrement}
\labch{dnb}

Tous les ensembles dans ce chapitre sont supposés finis.

\section{Principes fondamentaux}

\subsection{$+$, $\times$, $=$ et Dirichlet}

\begin{theorem}[Principe de multiplication]
    Soient $E_1, \ldots , E_n$ des ensembles finis. Alors 
    \[ |E_1 \times \ldots \times E_n| = |E_1| \times \ldots \times |E_n| \]
\end{theorem}

Pour exhiber l'utilité de ce principe, comptons le nombre de mots de taille $n$ sur un alphabet à 2 lettres. 
Prenons alors $M_n$ l'ensemble des mots de taille $n$ sur l'alphabet $\{0,1\}$. On constate
\begin{align*}
    M_n &= \left\{ (m_1, \ldots m_n) : m_i \in \{0,1\} \quad \forall i \in \llbracket 1,n \rrbracket \right\} \\
    &= \{0,1\} \times \cdots \times \{0,1\} \quad \text{ n fois } \\
    &= \{0,1\}^n. \\
    \text{Donc } |M_n| = |\{0,1\}|^n = 2^n. 
\end{align*}

\marginnote{Moins formellement, on peut interprêter cette situation comme 2 choix qu'on répète $n$ foix.
Ceci nous donne le même résultat de $2^n$ choix au total. \\
Plus généralement, le nombre de mots de longueur $n$ sur un alphabet à $k$ lettres est $k^n$.}

\begin{theorem}[Principe d'addition]
    Soient $E_1, \ldots, E_n$ des ensembles finis \emph{deux-à-deux disjoints}. Alors
    \[ E_1 \sqcup \ldots \sqcup E_n = |E_1| + \ldots + |E_n| \]
\end{theorem}

Pour illustrer ce principe, comptons le nombre de mots de longueur 1, 2, ou 3 sur un alphabet à 4 lettres.
Traitons donc $M_{\leq 3}$ l'ensemble des mots de longueur 1, 2 ou 3 sur $\{a,b,c,d\}$. On obtient, comme avant,
\begin{align*}
    M_{\leq 3} &= {\text{ mots de longueur 1 sur } \{a,b,c,d\} } \\
    &\sqcup {\text{ mots de longueur 2 sur } \{a,b,c,d\} } \\
    &\sqcup {\text{ mots de longueur 3 sur } \{a,b,c,d\} }.
\end{align*}

\marginnote{On utilise souvent le principe d'addition pour découper un ensemble en sous-ensembles disjoints vérifiant des 
propriétés mutuellement exclusives. \\ Si les sous-ensembles ne sont pas disjoints, le principe d'addition est remplacé par 
le principe d'inclusion-exclusion : \[ |A \cup B| = |A| + |B| - |A \cap B|. \] }

These principles may remind you of a notion of probability theory that I am certain we've all seen in high school:
the properties of probability trees! The following tree is an example I got off the internet where the sum of mutually
exclusive events is equal to 1 and the probability of successive events is equal to the product of the probabilities of
each individual event. To see this in action, try adding the probabilities of each "column": they're all equal to 1 (this 
is equivalent to the set partitions which we encounter with principle of addition). Furthermore, try multiplying the 
probabilities of each branch of a same "row": you should get the probablity of the end-node of that same "row". We will
see in subsequent courses that counting-- and in particular, \emph{enumeration}-- plays a big part in probability theory.

% The following tree was found on https://texample.net//tikz/examples/probability-tree/
% The author of this source code is Kjell Magne Fauske.

% Set the overall layout of the tree
\tikzstyle{level 1}=[level distance=3.5cm, sibling distance=3.5cm]
\tikzstyle{level 2}=[level distance=3.5cm, sibling distance=2cm]

% Define styles for bags and leafs
\tikzstyle{bag} = [text width=4em, text centered]
\tikzstyle{end} = [circle, minimum width=3pt,fill, inner sep=0pt]

% The sloped option gives rotated edge labels. Personally
% I find sloped labels a bit difficult to read. Remove the sloped options
% to get horizontal labels. 
\begin{tikzpicture}[grow=right, sloped]
\node[bag] {Bag 1 $4W, 3B$}
    child {
        node[bag] {Bag 2 $4W, 5B$}        
            child {
                node[end, label=right:
                    {$P(W_1\cap W_2)=\frac{4}{7}\cdot\frac{4}{9}$}] {}
                edge from parent
                node[above] {$W$}
                node[below]  {$\frac{4}{9}$}
            }
            child {
                node[end, label=right:
                    {$P(W_1\cap B_2)=\frac{4}{7}\cdot\frac{5}{9}$}] {}
                edge from parent
                node[above] {$B$}
                node[below]  {$\frac{5}{9}$}
            }
            edge from parent 
            node[above] {$W$}
            node[below]  {$\frac{4}{7}$}
    }
    child {
        node[bag] {Bag 2 $3W, 6B$}        
        child {
                node[end, label=right:
                    {$P(B_1\cap W_2)=\frac{3}{7}\cdot\frac{3}{9}$}] {}
                edge from parent
                node[above] {$B$}
                node[below]  {$\frac{3}{9}$}
            }
            child {
                node[end, label=right:
                    {$P(B_1\cap B_2)=\frac{3}{7}\cdot\frac{6}{9}$}] {}
                edge from parent
                node[above] {$W$}
                node[below]  {$\frac{6}{9}$}
            }
        edge from parent         
            node[above] {$B$}
            node[below]  {$\frac{3}{7}$}
    };
\end{tikzpicture}

\marginnote{Don't worry if you're confused by the tree; just know that these two principles reach quite far among different 
fields of mathematics.}

\begin{proposition}[Principe de Dirichlet]
    Soient $n, k \in \mathbb{Z}_+$ tels que $n > k$. Alors si $n$ éléments sont placés dans $k$ ensembles, 
    alors au moins un ensemble contient au moins 2 éléments.
\end{proposition}

\begin{proof}
    Par l'absurde, si chaque ensemble contenait au plus un élément, alors on aurait au plus k éléments au total.
\end{proof}

\begin{theorem}[Principe de Dirichlet généralisé]
    Soient $n \in \mathbb{Z}^\ast_+$ $k \in \mathbb{Z}_+^{\ast}$. Si $n$ éléments sont placés dans $k$ ensembles, 
    alors au moins un de ces ensembles contient \( \left\lceil \frac{n}{k} \right\rceil\) éléments.
\end{theorem}

\begin{proof}
    Par l'absurde, on suppose que chaque ensemble $E_i$ pour $i \in \llbracket 1, k \rrbracket$ contient au plus 
    \begin{align*}
        |E_i| &\leq \left\lceil \frac{n}{k} - 1 \right\rceil \text{ éléments } \\
        &< \frac{n}{k} 
    \end{align*}
    Au total, on aurait donc strictement moins de \( \frac{n}{k} \times k = n \) éléments. C'est une contradiction !
\end{proof}

\begin{theorem}[Principe d'égalité]
    S'il existe une bijection entre deux ensembles finis $E$ et $F$, alors $|E| = |F|$
\end{theorem}

\marginnote{Pas de preuve ??}

\begin{proposition}[Cardinalité de l'ensemble des parties]
    Soit $E$ un ensemble fini. On note \mathcal{ P(E) } l'ensemble de tous les sous-ensembles de $E$ (aussi appelés parties de $E$).
    Alors, \[ |\mathcal{P}(E)| = 2^n \text{ où } n = |E| \]. 
\end{proposition}

\begin{proof}
    On a vu que le cardinal de \( M_n = \{ \text{ mots de longueur n sur $\{0, 1\}$ } \} \) est $2^n$. On va essayer de trouver 
    une bijection entre $\mathscr{P}(E)$ et $M_n$.
    
    On commence par ordonner les éléments de $E$ : \( E = \{ x_1, \ldots, x_n \} \). Soit On définit l'application :
    
    \begin{align*}
        f : \mathscr{P}(E) &\to M_n \\
        F &\mapsto w_F \text{ où $w = l_1 \ldots l_n$ avec } l_i = \begin{cases} 1 & \text{ si } x_i \in F \\ 0 & \text{ sinon} \end{cases}
    \end{align*}

    $f$ est bien une bijection. Pour le prouver on donne la \emph{bijection réciproque} :

    \begin{align*}
        f^{-1} : M_n &\to \mathscr{P}(E) \\
        w &\mapsto F_w \text{ où } \{ x_i \in E \mid l_i = 1 \}
    \end{align*}
\end{proof}


\subsection{Double comptage}

Ce principe est une techinque consistant à calculer une quantité combinatoire de deux manières différentes ce qui permet d'obtenir des identités.

\begin{definition}[Système d'incidence]
    Un \emph{système d'incidence} $(E,F,\mathcal{R})$ consiste en deux ensembles finis $E$ et $F$ et une relation $\mathcal{R}$ (appelée \emph{relation d'incidence}) 
    entre $E$ et $F$. S'il existe une relation $x\mathcal{R}y$ entre $X \in E$ et $y \in F$, on dit que $x$ et $y$ sont incidents. 
\end{definition}

\begin{theorem}
    Soit $(E,F,\mathcal{R})$ un système d'incidence. Pour tout $x \in E$, on note $\mathcal{r}^{+}(x)$ le nombre d'éléments $y \in F$ incidents à $x$. De même, pour tout
    $y \in F$, on note $\mathcal{r}^{-}(y)$ le nombre d'éléments $x \in E$ incidents à $y$. Alors, 
    \[ \sum_{x \in E} \mathcal{r}^{+}(x) = \sum_{y \in F} \mathcal{r}^{-}(y) \].
\end{theorem}


\section{Combinatoire énumérative classique}

\subsection{Combinaisons}

\begin{definition}[Coefficient binomial]
\end{definition}


\begin{proposition}[Triangle de Pascal]
\end{proposition}


\begin{proposition}[Formule du binôme]
\end{proposition}


\begin{proposition}[Binôme de Newton]
\end{proposition}


\subsection{Permutations}

\begin{definition}[Arrangement]
\end{definition}


\begin{definition}[Permutation]
\end{definition}


\begin{proposition}[Formule d'arrangement]
\end{proposition}


\begin{proposition}[Formule de combinaison]
\end{proposition}


\subsection{Distributions}

\begin{definition}[Coefficient multinomial]
\end{definition}

\begin{proposition}[Formule de distribution]
\end{proposition}

\begin{theorem}[Théorème multinomial]
\end{theorem}

\begin{corollary}[Formule du multinôme]
\end{corollary}


\subsection{Partitions}

\begin{definition}[Partition d'un ensemble]
    Soit $E$ un ensemble. Un ensemble $\{ E_i : i \in I \}$ de sous-ensembles $E_i$ \emph{non-vides} tels que 
    \( \bigsqcup_{i \in I} E_i = E \) est une \emph{partition de l'ensemble $E$}, et les sous-ensembles $E_i$ sont appelées
    les \emph{parties} de la partition.
\end{definition}

\marginnote{Soit $E = \{a,b,c\}$. \\ Les partitions de $E$ sont
\begin{description}
    \item[1 partie] \( \left\{ \{a,b,c\} \right\} \)
    \item[2 parties] \( \left\{a, \{b,c\} \right\}, \left\{ \{a,b\}, c\right\}, \left\{ \{a,c\}, b\right\} \)
    \item[3 parties] \( \left\{ \{a\}, \{b\}, \{c\} \right\} \)
\end{description}}

\begin{definition}[$S(n,k)$]
    Soient $0 \leq k \leq n$ deux entiers positifs. Le \emph{nombre de partitions d'un ensemble à $n$ éléments en k parties}
    est noté \emph{$S(n,k)$} et appelé \emph{nombre de Stirling de seconde espèce.} 
\end{definition}

\begin{proposition}[Propriétés de $S(n, k)$]
    \hfill \break
    \begin{align}
        \forall n \geq 1, \quad &S(n,0) = 0 \\
        \forall k > n, \quad &S(n,k) = 0 \\
        \forall n \geq 1, \quad &S(n,1) = S(n,n) = 1 \\
        \forall 2 \leq k \leq n-1, \quad &S(n,k) = S(n-1, k-1) + k \cdot S(n-1,k)
    \end{align}
\end{proposition}

\marginnote{Par convention, il existe une seule partition d'un ensemble à 0 éléments en 0 parties. C'est-à-dire 
$S(0,0) = 1$}

\begin{proof}
    \hfill \break
    \begin{enumerate}
        \item Si $n \geq 1$, il n'y a aucune partition d'un ensemble à $n$ éléments en $0$ parties.
        \footnote[1]{I really dislike this proof. This is merely a restatement of the result, not a demonstration of its validity.
        I find it much more elucidating to walk through the actual reasoning, which I will attempt to exhibit as follows. \\
        \indent \textit{In the naïve set theory we have been exposed to in last semester's course as well as in the preliminary chapter of this one,
        we saw the \emph{axiom of existence}. This foundational axiom claims that there exists a \emph{unique} set called $\emptyset$-- or empty set--
        which has no parts. However, a set $E$ with $n$ elements where $n \geq 1$ is not empty. And yet we are tasked with finding 
        sets $E_i$ such that $\bigcup E_i = E$ where each $E_i$ has no parts. This is absurd! $\bigcup \emptyset = \emptyset$ and
        $E \neq \emptyset$. Hence there are no ways of partitioning an nonempty set into empty parts.} \\
        \indent I believe this proof is better suited than the one given in class.}
        
        \item Par définition d'une partition, les parties sont \emph{non-vides}! Il est donc impossible d'avoir plus de parties que
        la taille de l'ensemble partitionné.
        
        \item Soit $n \geq 1$. Un ensemble de taille $n$ a une seule partition en une seule partie (lui-même) et une seule partition
        en $n$ parties (composé de $n$ singletons).
        \footnote[2]{Like before, this "proof" is shallow. At the risk of being pedantic, I suggest the following reasoning instead. \\
        \indent \textit{A set is an unordered collection of elements and a part of a partition is a set. Therefore, the partition of a set 
        with $n$ elements in $n$ parts is simply the set of singletons of its elements. Similarly, since parts hold the same properties as wholes,
        the only way to partition a set with $n$ elements in $1$ part is to take the (whole) set itself.} \\
        \indent I find the above proof more appropriate than that which was given.}
        
        \item Soit $E = \{x_1, \ldots, x_n\}$ un ensemble de taille $n$.
        
    \end{enumerate}
\end{proof}


\begin{definition}[Nombre de Bell]
\end{definition}

\begin{definition}[Partition entière]
\end{definition}

\begin{definition}[Partition conjuguée]
\end{definition}


\subsection{Combinaisons avec répétitions}

\begin{definition}[Multi-ensemble]
    Un \emph{multi-ensemble} est un ensemble dans lequel les éléments peuvent apparaître plusieurs fois. Il se note entre
    doubles accolades $\{\{ \ldots \}\}$.
\end{definition}

\begin{definition}[Combinaison avec répétitions]
    Une \emph{combinaison avec répétitions de $k$ éléments parmi $n$} est un multi-ensemble de taille $k$ sur un ensemble $E$
    à $n$ éléments. Le nombre de combinaisons avec répétition de $k$ éléments parmi $n$ est noté $\multiset{n}{k}$.
\end{definition}
\marginnote{$\multiset{n}{k}$ est le nombre de façons de placer $k$ balles non distinguables dans $n$ boîtes distinguables.}

\begin{proposition}[Stars and Bars Property for Multiset Combinations]
    \[ \forall n,k \geq 0, \quad \multiset{n}{k} = \binom{n+k-1}{k} \]
\end{proposition}

\subsection{Compositions}

\begin{definition}[Composition]
    Une \emph{composition} d'un entier $n \geq 1$ est une suite $(c_1, \ldots, c_k)$ d'entiers strictement positifs, appelés
    parts, dont la somme vaut $n$. On note $c_k(n)$ le nombre de compositions de $n$ en $k$ parts.
\end{definition}

\marginnote{Choisir une composition de $n$ en $k$ parts revient à placer $n$ balles non distinguables dans $k$ boîtes distinguables
tel qu'il y ait au moins une balle par boîte. De manière équivalente, on peut aussi dire qu'il y a $i$ balles dans la boîte $j$
ssi la j-ème part est égale à $i$.}

\begin{proposition}[Stars and Bars Property for Strong Integer Composition]
    \[ \forall n,k \geq 1, c_k(n) = \binom{n-1}{k-1}. \]
\end{proposition}


\begin{corollary}[Total Compositions Theorem]
    Le nombre total de composition de $n$ est $2^{n-1}$.
\end{corollary}

\begin{definition}[Composition faible]
    Une \emph{composition faible} de $n$ est une suite $(c_1, \ldots, c_k)$ d'entiers positifs ou nuls tels que leur somme
    vaut $n$.
\end{definition}

\begin{proposition}[Total Weak Compositions Theorem]
    Le nombre de compositions faibles de $n$ en $k$ parts est $\multiset{k}{n}$.
\end{proposition}


\subsection{\guillemetleft The Twelvefold Way \guillemetright}

Nous allons résumer tout ce que nous avons vu dans ce chapitre, en particulier les interprétations en termes de placement de balles dans des boîtes dans un tableau
à douze cases.

Soit $B$ un ensemble fini de taille $n$ (les balles) et $X$ un ensemble fini de taille $k$ (les boîtes). Un placement des $n$ balles dans les $k$ boîtes. Un placement des
$n$ balles dans les $k$ boîtes correspond donc à une application $f : B \to X$. Comme avant, les balles et les boîtes peuvent être distinguiables ou indistinguables. 
De plus l'application $f$ peut être injective (au plus une balle par boîte), surjective (au moins une balle par boîte) ou quelconque.

\begin{center}
    \begin{tabular}{|c||c|c|c||} 
     \hline
      & $f$ quelconque & $f$ injective & $f$ surjective \\ [0.5ex] 
     \hline\hline
     B et X distinguables & \numcell{$k^n$}{1} & \numcell{$A(k,n)$}{2} & \numcell{$k! \cdot S(n,k)$}{3} \\ 
     \hline
     X distinguable & \numcell{\( \multiset{k}{n} = \binom{n+k-1}{n} \)}{4} & \numcell{\( \binom{k}{n} \)}{5} & \numcell{\[ c_k(n) = \binom{n-1}{k-1} \]}{5} \\
     \hline
     B distinguable & \numcell{\[ \sum_{i=1}^{k} S(n, i) \]}{7} & \numcell{\begin{cases}
        0 & \text{si } n > k \\
        1 & \text{si } n \leq k
     \end{cases}}{8} & \numcell{$S(n,k)$}{9} \\
     \hline
     B et X indistinguables & \numcell{\[ \sum_{i=1}^{k} p_i(n) \]}{10} & \numcell{\begin{cases}
        0 & \text{si } n > k \\
        1 & \text{si } n \leq k
     \end{cases}}{11} & \numcell{$p_k(n)$}{12} \\ [1ex] 
     \hline
    \end{tabular}
\end{center}

\begin{proof}
    1. On place $n$ balles distinguables dans $k$ boîtes distinguables de manière quelconque. Cela revient à choisir pour
    chaque balle dans quelle boîte elle sera (il y a $k$ choix). Par le principe de multiplication, il y a $k^n$ possibilités
    au total. 

    3. On a vu en (9) que $S(n,k)$ est le nombre de manières de placer $n$ balles distinguables dans $k$ boîtes indistinguiables.
    Si maintenant les boîtes sont distinguables aussi, ce qui revient à les ordonner/numéroter, alors chaque placement en (9)
    donne $k!$ placements en ordonnant les boîtes. On a donc bien $k! \cdot S(n,k)$ possibilités au total.

    7. Dans le cas (9), $f$ devait être surjective, donc chacune des $k$ boîtes devait contenir au moins une balle. Si maintenant
    $f$ est quelconque, certaines boîtes peuvent rester vides. Donc au lieu de compter les partitions de $\{ 1, \ldots, n \}$ en
    exactement $k$ parties, on compte les partitions en au plus $k$ parties. Par le principe d'addition, il y en a 
    \( \sum_{i=1}^{k} S(n,i) \).

    10. De la même manière, si on enlève la surjectivité dans (12), alors certaines boîtes peuvent être vides et on compte les
    partitions de $n$ en \emph{au plus} $k$ parts. Il y en a \( \sum_{i=1}^{k} \mathcal{p}_i(n) \).

    8 et 11. Si $n > k$, il y a plus de balles que de boîtes, et d'après le principe de Dirichlet, il n'y a aucun placement avec 
    au plus une balle par boîte. Si $n \leq k$, on place chaque balle dans une boîte différente. Comme les boîtes sont indistinguables,
    tous ces placements sont équivalents. Il y a donc une seule possibilité. 
    
    
\end{proof}