\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax
\babel@toc {french}{}\relax
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\nonumberline Table des mati\`eres}{3}{chapter*.1}%
\defcounter {refsection}{0}\relax
\contentsline {part}{\nonumberline Cours}{1}{part*.3}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\numberline {1}Dénombrement}{3}{chapter.1}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {1.1}Principes fondamentaux}{3}{section.1.1}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsection}{\numberline {1.1}Principes fondamentaux}{3}{section.1.1}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\numberline {1.1.1}$+$, $\times $, $=$ et Dirichlet}{3}{subsection.1.1.1}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsubsection}{\numberline {1.1.1}$+$, $\times $, $=$ et Dirichlet}{3}{subsection.1.1.1}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\numberline {1.1.2}Double comptage}{5}{subsection.1.1.2}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsubsection}{\numberline {1.1.2}Double comptage}{5}{subsection.1.1.2}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {1.2}Combinatoire énumérative classique}{5}{section.1.2}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsection}{\numberline {1.2}Combinatoire énumérative classique}{5}{section.1.2}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\numberline {1.2.1}Combinaisons}{5}{subsection.1.2.1}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsubsection}{\numberline {1.2.1}Combinaisons}{5}{subsection.1.2.1}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\numberline {1.2.2}Permutations}{6}{subsection.1.2.2}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsubsection}{\numberline {1.2.2}Permutations}{6}{subsection.1.2.2}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\numberline {1.2.3}Distributions}{6}{subsection.1.2.3}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsubsection}{\numberline {1.2.3}Distributions}{6}{subsection.1.2.3}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\numberline {1.2.4}Partitions}{6}{subsection.1.2.4}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsubsection}{\numberline {1.2.4}Partitions}{6}{subsection.1.2.4}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\numberline {1.2.5}Combinaisons avec répétitions}{7}{subsection.1.2.5}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsubsection}{\numberline {1.2.5}Combinaisons avec répétitions}{7}{subsection.1.2.5}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\numberline {1.2.6}Compositions}{7}{subsection.1.2.6}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsubsection}{\numberline {1.2.6}Compositions}{7}{subsection.1.2.6}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\numberline {1.2.7}\guillemetleft The Twelvefold Way \guillemetright }{8}{subsection.1.2.7}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsubsection}{\numberline {1.2.7}\guillemetleft The Twelvefold Way \guillemetright }{8}{subsection.1.2.7}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\numberline {2}Principe d'inclusion/exclusion}{11}{chapter.2}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {2.1}Cas particuliers simples}{11}{section.2.1}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsection}{\numberline {2.1}Cas particuliers simples}{11}{section.2.1}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {2.2}Généralisation}{12}{section.2.2}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsection}{\numberline {2.2}Généralisation}{12}{section.2.2}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {2.3}Exemples d'applicaiton}{13}{section.2.3}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsection}{\numberline {2.3}Exemples d'applicaiton}{13}{section.2.3}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\numberline {2.3.1}Divisibilité}{13}{subsection.2.3.1}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsubsection}{\numberline {2.3.1}Divisibilité}{13}{subsection.2.3.1}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\numberline {2.3.2}Dérangements}{14}{subsection.2.3.2}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsubsection}{\numberline {2.3.2}Dérangements}{14}{subsection.2.3.2}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\numberline {3}Séries génératrices}{15}{chapter.3}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {3.1}L'anneau des séries formelles}{15}{section.3.1}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsection}{\numberline {3.1}L'anneau des séries formelles}{15}{section.3.1}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {3.2}Séries formelles à plusieurs variables}{17}{section.3.2}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsection}{\numberline {3.2}Séries formelles à plusieurs variables}{17}{section.3.2}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {3.3}Les séries génératrices en combinatoire}{18}{section.3.3}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsection}{\numberline {3.3}Les séries génératrices en combinatoire}{18}{section.3.3}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {3.4}Calculs de séries génératrices classiques}{21}{section.3.4}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsection}{\numberline {3.4}Calculs de séries génératrices classiques}{21}{section.3.4}%
\defcounter {refsection}{0}\relax
\contentsline {subsection}{\numberline {3.4.1}Partitions d'entiers}{23}{subsection.3.4.1}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsubsection}{\numberline {3.4.1}Partitions d'entiers}{23}{subsection.3.4.1}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{\numberline {4}Théorie des graphes}{25}{chapter.4}%
\defcounter {refsection}{0}\relax
\contentsline {section}{\numberline {4.1}Notions de base}{25}{section.4.1}%
\defcounter {refsection}{0}\relax
\contentsline {mtocsection}{\numberline {4.1}Notions de base}{25}{section.4.1}%
\defcounter {refsection}{0}\relax
\contentsline {part}{\nonumberline Séries}{27}{part*.7}%
\defcounter {refsection}{0}\relax
\contentsline {chapter}{Série 1}{29}{part*.7}%
\defcounter {refsection}{0}\relax
\contentsline {section}{Exercice 1}{29}{chapter*.8}%
\defcounter {refsection}{0}\relax
\contentsline {section}{Exercice 2}{29}{section*.11}%
\defcounter {refsection}{0}\relax
\contentsline {section}{Exercice 3}{30}{section*.14}%
\defcounter {refsection}{0}\relax
\contentsline {section}{Exercice 4}{31}{section*.17}%
\defcounter {refsection}{0}\relax
\contentsline {section}{Exercice 5}{31}{section*.20}%
