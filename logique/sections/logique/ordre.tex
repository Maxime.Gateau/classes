\setchapterstyle{kao}
\setchapterpreamble[u]{\margintoc}
\chapter{Les relations d'ordre}
\labch{ordre}

\section{Notions de base}

\begin{definition}
    Soit E un ensemble. Une relation sur $E$, notée $\mathscr{R}$, est un prédicat prenant deux arguments
    dans E et renvoyant V ou F.
\end{definition}

\begin{definition}
    Soit E un ensemble. $\mathscr{R}$ est une relation d'ordre si :
    \begin{description}
        \item[Réflexivité] \[ \forall x \in E, x \mathscr{R} x \]
        \item[Transitivité] \[ \forall x, y, z \in E, x \mathscr{R} y \text{ et } y \mathscr{R} z \implies x \mathscr{R} z \]
        \item[Antisymétrie] \[ \forall x, y \in E, x \mathscr{R} y \text{ et } y \mathscr{R} x \implies x = y \]
    \end{description}
\end{definition}

\begin{definition}
    Un ensemble $E$ muni d'une relation d'ordre $\leq$ est appelé un \emph{ensemble ordonné}. On note
    \[ (E, \leq) \]
\end{definition}

\begin{definition}
    Une relation d'ordre $\mathscr{R}$ sur $E$ est dite \emph{totale} si \[ \forall x, y \in E, x \mathscr{R} y \text{ et } y \mathscr{R} x \]
    Sinon, la relation d'ordre est dite partielle.
\end{definition}

\section{Majorant, minorant, sup, inf, max, min}

\begin{definition}
    Soit $(E, \leq)$ un ensemble ordonné et $F \subset E$ et $m, M \in E$.
    \begin{enumerate}
        \item $M$ est un \emph{majorant} de $F$ si \[ \forall x \in F, x \leq M \]
        \item $M$ est le \emph{supremum} de $F$ si \[ \forall x \in F, x \leq M \text{ et } \forall M' \in E, (\forall x \in F, x \leq M') \implies M \leq M' \]
        \item $M$ est le \emph{maximum} de $F$ si \[ \forall x \in F, x \leq M \text{ et } \exists x \in F, x = M \]
        \item $m$ est un \emph{minorant} de $F$ si \[ \forall x \in F, m \leq x \]
        \item $m$ est l'\emph{infimum} de $F$ si \[ \forall x \in F, m \leq x \text{ et } \forall m' \in E, (\forall x \in F, m' \leq x) \implies m' \leq m \]
        \item $m$ est le \emph{minimum} de $F$ si \[ \forall x \in F, m \leq x \text{ et } \exists x \in F, x = m \]
    \end{enumerate}
\end{definition}

\begin{proposition}
    Lorsqu'il existe, le supremum/infinumum/maximum/minimum de F est unique.
\end{proposition}

Les propriétés suivantes sont valables sous réserve d'existence.

\begin{proposition}
    Soient $(E, \leq)$ un ensemble ordonné et $F \subset G \subset E$. On a
    \[ \sup F \leq \sup G \text{ et } \inf F \geq \inf G \].
    De même, si ${x_i}_{i \in I}$ est une famille d'éléments de $E$ et $J \subset I$, alors
    \[ \sup_{j \in J} x_j \leq \sup_{i \in I} x_i \text{ et } \inf_{j \in J} x_j \geq \inf_{i \in I} x_i \]
\end{proposition}

\begin{proposition}
    Soit $(E, \leq)$ ordonné. Soit $(x_i)_{i \in I} \in E^{I}$ et $(J_k)_{k \in K}$ un recouvrement de I tel que
    $\bigcup_{k \in K} J_k = I$. Alors 
    \[ \sup_{i \in I} x_i = \sup_{k \in K} \sup_{j \in J_k} x_j \text{ et } \inf_{i \in I} x_i = \inf_{k \in K} \inf_{j \in J_k} x_j \]
\end{proposition}

\begin{proposition}
    Soient $F \subset \mathbb{R}$ et $M \in \mathbb{R}$.
    \[ M = \sup F \iff \forall x \in F, (x \leq M \text{ et } \forall y \in \mathbb{R}, y < M) \implies \exists x \in F, y < x \].
\end{proposition}

\section{Applications croissantes, décroissantes et monotones}

\begin{definition}
    Soient $(E, \leq)$ et $(F, \leq)$ deux ensembles ordonnés.
    \begin{align*}
        f : E \to F \text{ est dite croissante si } &\forall x, y \in E, x \leq y \implies f(x) \leq f(y) \\
        \text{ et décroissante si } &\forall x, y \in E, x \leq y \implies f(x) \geq f(y)
    \end{align*}
    Enfin, $f$ est monotone si $f$ est croissante ou décroissante.
\end{definition}

\begin{proposition}
    Soient $(E, \leq)$, $(F, \leq)$ et $(G, \leq)$ trois ensembles ordonnés et $f : E \to F$ et $g : F \to G$ deux applications.
    \begin{itemize}
        \item Si $f$ et $g$ sont croissantes, alors $g \circ f$ est croissante.
        \item Si $f$ et $g$ sont décroissantes, alors $g \circ f$ est croissante.
    \end{itemize}
\end{proposition}

\begin{definition}
    Soit $f : X \to E$, où $(E, \leq)$ est un espace ordonné. On dit que $f$ est majorée s'il existe $M \in E$ tel que \[ \forall x \in X, f(x) \leq M \]
    On appelle $\sup f$ le plus petit majorant de $f$.
\end{definition}