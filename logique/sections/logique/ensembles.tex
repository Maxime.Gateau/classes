\setchapterstyle{kao}
\setchapterpreamble[u]{\margintoc}
\chapter{Notion mathématique d'ensemble}
\labch{ensembles}

\section{Définitions préliminaires}

\begin{definition}[Ensemble]
    Un ensemble est une \emph{collection d'éléments}. Pour un élément $x$ d'un ensemble $E$,
    on note $x \in E$ et on lit \og $x$ appartient à $E$ \fg{}. Si $x$ n'est pas un élément de $E$,
    on note $x \notin E$ et on lit \og $x$ n'appartient pas à $E$ \fg{}.
\end{definition}

\begin{definition}[Inclusion]
    Soient $E$ et $F$ deux ensembles. On dit que $E$ est \emph{inclus} dans $F$ si tout élément de $E$
    est un élément de $F$. On note alors $E \subset F$. 
    Si $E$ n'est pas inclus dans $F$, on note $E \not\subset F$ et on lit \og $E$ n'est pas inclus dans $F$ \fg{}.
\end{definition}

\begin{proposition}[Transitivité de l'inclusion]
    Si $E$, $F$ et $G$ sont trois ensembles tels que $E \subset F$ et $F \subset G$, alors $E \subset G$.
\end{proposition}

\begin{proof}
    Soit $x \in E$, montrons $x \in G$. Puisque $E \subset F$, on en déduit que $x \in F$. Puisque $F \subset G$, 
    on en déduit que $x \in G$.
\end{proof}


\section{Axiomatique de la théorie des ensembles}

\begin{axiom}[Axiome d'existence]
    Il existe un \emph{unique} ensemble appelé \emph{ensemble vide} et noté $\emptyset$ tel que quelque soit l'objet $x$,
    $x \notin \emptyset$.
\end{axiom}

\begin{axiom}[Axiome de compréhension]
    Soit $E$ un ensemble et $A(x)$ une famille d'assertions indexée par E. Il existe un unique ensemble constitué
    des éléments de $E$ tel que $A(x)$ est vraie. On note
    \[ F = \{ x \in E \text{tel que} A(x) \} \coloneqq \{ x \in E, A(x) \} \]
\end{axiom}

\begin{axiom}[Axiome de puissance]
    Soit $E$ un ensemble. Il existe un \emph{unique} ensemble composé de tous les ensembles $F$ inclus dans $E$.
    Ces ensembles sont appelés les \emph{parties} de $E$, et cet ensemble des parties est noté
    \[ \mathcal{P}(E) \coloneqq \{ F, F \subset E \} \]
\end{axiom}

\begin{axiom}[Axiome de l'union]
    Soient $E$ et $F$ deux ensembles. Il existe un \emph{unique} ensemble noté $E \cup F$ composé des éléments
    de $E$ et des éléments de $F$.
\end{axiom}

\begin{proposition}[Propriétés de l'union]
    Soient $E$, $F$ et $G$ trois ensembles. On a
    
    \begin{description}
        \item[Associativité] $E \cup (F \cup G) = (E \cup F) \cup G$
        \item[Commutativité] $E \cup F = F \cup E$
        \item[Élément neutre] $E \cup \emptyset = E$
    \end{description}
\end{proposition}


\section{Intersection et complémentaire}

\begin{definition}[Intersection et complémentaire]
    Soient $E$ et $F$ des ensembles. On définit
    \begin{align*}
        E \cap F &\coloneqq \{ x \in E, x \in F \} \\
        E \setminus F &\coloneqq \{ x \in E, x \notin F \}
    \end{align*}
\end{definition}

\begin{remark}
    Lorsque $F \subset E$, $E \setminus F$ est le complémentaire de $F$. Si $E$ est complètement 
    évident dans le contexte, on notera $F^c$ au lieu de $E \setminus F$.
\end{remark}

\begin{remark}
    Si $E \cap F = \emptyset$, on dit que $E$ et $F$ sont disjoints.
\end{remark}

\begin{proposition}[Propriétés de l'intersection]
    \begin{description}
        \item[Associativité] $E \cap (F \cap G) = (E \cap F) \cap G$
        \item[Commutativité] $E \cap F = F \cap E$
        \item[Élément destructeur] $E \cap \emptyset = \emptyset$ 
    \end{description}
\end{proposition}

\begin{proposition}
    Soient $E$, $F$ et $G$ trois ensembles.
    \begin{description}
        \item[Distributivité de l'intersection sur l'union] $(E \cap F) \cup (E \cap G) = E \cap (F \cup G)$
        \item[Distributivité de l'union sur l'intersection] $(E \cup F) \cap (E \cup G) = E \cup (F \cap G)$
    \end{description}
\end{proposition}

\begin{proposition}
    Soient $E$ et $F$, et $G$ trois ensembles.
    \begin{description}
        \item[Distributivité du complémentaire sur l'union] $E \setminus (F \cup G)$
        \item[Distributivité du complémentaire sur l'intersection] $E \setminus (F \cap G)$
    \end{description}
\end{proposition}

\begin{definition}
    Considérons une famille d'ensembles $( A_i )_{i \in I}$ tel que $A_i \subset E$ pour tout $i \in I$.
    Soit $F \subset E$. On a que
    \begin{itemize}
        \item Les $A_i$ forment un \emph{recouvrement} de $F$ si $F \subset \bigcup_{i \in I} A_i$.
        \item Si $F = \bigcup_{i \in I} A_i$ et $A_i \cap A_j = \emptyset$ pour tout $i \neq j$, alors
        $\bigcup_{i \in I} A_i$ est une \emph{partition} de $F$.
    \end{itemize}
\end{definition}

\begin{definition}
    Soit $I$ un ensemble et, incluse dans E, $(A_i)_{i \in I}$ une famille d'ensembles. On définit
    \begin{itemize}
        \item $\bigcup_{i \in I} A_i \coloneqq \{ x \in E, \exists i \in I, x \in A_i \}$ et
        \item $\bigcap_{i \in I} A_i \coloneqq \{ x \in E, \forall i \in I, x \in A_i \}$
    \end{itemize}
\end{definition}


\section{Produit cartésien et n-uplets}

\begin{definition}
    Soient $E$ et $F$ deux ensembles. On définit le \emph{produit cartésien} de $E$ et $F$ comme étant
    \[ E \times F \coloneqq \{ (x, y), x \in E, y \in F \} \]
    Plus généralement, on introduit
    \begin{align*}
        E_1 \times E_2 \times \cdots \times E_n &\coloneqq \{ (x_1, x_2, \ldots, x_n) : x_i \in E_i, \text{pour tout } i \in \llbracket 0, n \rrbracket \} \\
        &= E_1 \times (E_2 \times ( \cdots \times (E_{n-1} \times E_n) \cdots ))
    \end{align*}
\end{definition}

\begin{remark}
    $E \times F$ est \mathit{a priori} différent de $F \times E$ (sauf si $E = F$).
    En effet, la propriété caractéristique du produit cartésien est la suivante:
    \[ (x, y) = (x', y') \iff x = x' \text{ et } y = y' \]
\end{remark}