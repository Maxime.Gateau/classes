\setchapterstyle{kao}
\setchapterpreamble[u]{\margintoc}
\chapter{Logique élémentaire}
\labch{logique}

\section{Calcul prépositionnel}

Soit $\mathcal{A}$ un ensemble de proposition atomiques.

\begin{definition}
    L'ensemble des familles propositionnelles (dépendant de $\mathcal{A}$) est défini comme le plus petit
    ensemble (pour inclusion) $\mathcal{L}$ tel que:
    \begin{enumerate}
        \item $\mathcal{A} \subset \mathcal{L}$
        \item Si $\mathcal{A} \in \mathcal{L}$, alors $\lnot \mathcal{A} \in \mathcal{L}$
        \item Si $\mathcal{A}, \mathcal{B} \in \mathcal{L}$, alors $\mathcal{A} \land \mathcal{B} \in \mathcal{L}$ 
        et $\mathcal{A} \lor \mathcal{B} \in \mathcal{L}$
    \end{enumerate}
\end{definition}

\begin{definition}[Valuation]
    Une solution $\varphi$ est une fonction qui à tout élément $A \in \mathcal{A}$ attribue une \emph{valeur}
    VRAIE ou FAUSSE.
\end{definition}

Si $A$ et $B$ ont une valeur VRAIE ou FAUSSE, la valeur de $\lnot A$, $A \land B$ et $A \lor B$
sont données comme suit:

\begin{minipage}[c]{0.5\textwidth}
    \centering
    \begin{tabular}{c c}
        \toprule%%–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
        $A$ & $\lnot A$ \\
        \midrule%%–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
        V & F \\ \hdashline%%··········································
        F & V \\ \bottomrule%––––––––––––––––––––––––––––––––––––––––––
    \end{tabular}
    \captionof{table}{Table de vérité de $\lnot$}
\end{minipage}
\begin{minipage}[c]{0.5\textwidth}
    \centering
    \begin{tabular}{c c c c}
        \toprule%%–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
        $A$ & $B$ & $A \land B$ & $A \lor B$ \\
        \midrule%%–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
        V & V & V & V \\ \hdashline%%··········································
        V & F & F & V \\ \hdashline%%··········································
        F & V & F & V \\ \hdashline%%··········································
        F & F & F & F \\ \bottomrule%––––––––––––––––––––––––––––––––––––––––––
    \end{tabular}
    \captionof{table}{Table de vérité de $\land$ et $\lor$}
\end{minipage}


% EXAMPLE TABLE!
% \begin{table}[htbp!]
%     \centering
%     \begin{tabular}{@{}cccc@{}}
%         \toprule%%–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
%         $A$ & $B$ & $(A \oplus B)'$ & $(A) \oplus (B')$ \\
%         \midrule%%–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
%         0 & 0 & 1 & 1 \\ \hdashline%%··········································
%         0 & 1 & 0 & 0 \\ \hdashline%%··········································
%         1 & 0 & 0 & 0 \\ \hdashline%%··········································
%         1 & 1 & 1 & 1 \\ \bottomrule%––––––––––––––––––––––––––––––––––––––––––
%     \end{tabular}
% \end{table}

En utilisant ces règles, on étend les valeurs V et F à toutes les formules propositionnelles.

\begin{definition}[Tautologie]
    Une tautologie est une formule propositionnelle qui est \emph{vraie quelle que soit $\varphi$}.
\end{definition}

\begin{proposition}[Lois de De Morgan]
    Soient $\mathcal{A}, \mathcal{B} \in \mathcal{L}$, alors
    \begin{align*}
        \lnot (A \lor B) &\iff (\lnot A) \land (\lnot B) \\
        \lnot (A \land B) &\iff (\lnot A) \lor (\lnot B)
    \end{align*}
\end{proposition}

On définit l'implication logique $\implies$ comme suit.

\begin{minipage}[c]{0.5\textwidth}
    \centering
    \begin{tabular}{c c c}
        \toprule%%–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
        $A$ & $B$ & $A \implies B$ \\
        \midrule%%–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
        V & V & V \\ \hdashline%%··········································
        V & F & F \\ \hdashline%%··········································
        F & V & V \\ \hdashline%%··········································
        F & F & V \\ \bottomrule%––––––––––––––––––––––––––––––––––––––––––
    \end{tabular}
    \captionof{table}{Table de vérité de $\implies$}
\end{minipage}

\begin{proposition}
    Les assertions suivantes sont équivalentes:
    \begin{enumerate}
        \item $A \implies B$
        \item $\lnot A \lor B$
        \item $\lnot B \implies \lnot A$
        \item $\lnot (A \land \lnot B)$
    \end{enumerate}
\end{proposition}

\begin{proposition}
    Soient $\mathcal{A}, \mathcal{B} \in \mathcal{L}$. Les assertions suivantes sont équivalentes:
    \begin{enumerate}
        \item $\mathcal{A} \iff \mathcal{B}$
        \item $(\mathcal{A} \implies \mathcal{B}) \land (\mathcal{B} \implies \mathcal{A})$
    \end{enumerate}
\end{proposition}

\section{Calcul des prédicats}

\begin{definition}[Prédicat]
    Considérons un ensemble de symboles appelés \emph{variables}. Un prédicat prend en entrée une ou plusieurs variables
    et rend V ou F.
\end{definition}

\begin{definition}
    Soit $A(x, y, \ldots )$ un prédicat.
    \begin{itemize}
        \item Le prédicat $\forall x \in E, A(x, y, \ldots)$ signifie \og pour tout $x \in E, A(x, y, \ldots)$ \fg{}
        et $\forall$ est le \emph{quantificateur universel}.
        \item Le prédicat $\exists x \in E, A(x, y, \ldots)$ signifie \og il existe $x \in E, A(x, y, \ldots)$ \fg{}
        et $\exists$ est le \emph{quantificateur existentiel}.
    \end{itemize}
\end{definition}

L'ordre des quantificateurs importe! Mais on peut échanger deux $\forall$ qui se suivent (idem pour $\exists$).

