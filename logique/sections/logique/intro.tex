\setchapterstyle{kao}
\setchapterpreamble[u]{\margintoc}
\chapter{Introduction}
\labch{intro}

\section{Notions préliminaires}

\begin{definition}[Énoncé mathématique]
    Un énoncé mathématique est une phrase \emph{syntaxiquement correcte au sens mathématique} à laquelle
    on peut associer une valeur \emph{VRAIE (V)} ou \emph{FAUSSE (F)}.
\end{definition}

\marginnote{
    $2 + 2 = 4$, $0 = 1$ et \textit{"Ma maison est en feu."} sont tous des énoncés mathématiques.
    En effet, que l'énoncé soit écrit en symboles mathématique ou en français, un énoncé mathématique
    c'est juste une \emph{phrase déclarative}, ou une affirmation.
}

\begin{definition}[Axiome]
    Un axiome est un énoncé mathématique \emph{supposé vrai} sans démonstration.
\end{definition}

Euclid's postulates, axiom of choice, axiom of real determinacy (game theory) which is incompatible with the axiom of choice

\begin{definition}[Théorème, proposition, corollaire, lemme]
    Ce sont des énoncés mathématiques \emph{démontrés comme vrais} à partir d'axiomes.
    \begin{description}
        \item[Théorème] résultat de grande importance
        \item[Proposition] résultat de moindre importance
        \item[Corollaire] conséquence (relativement facile) d'un théorème
        \item[Lemme] résultat intermédiaire dans une preuve
    \end{description}
\end{definition}

Dans le paragraphe qui suit nous verrons un exemple de chacun de ces énoncés mathématiques à travers une notion qui devrait vous être parfaitement
familières à ce stade : celle de \emph{dimension} d'espaces vectoriels en algèbre linéaire.


Dans les théorèmes fondamentaux sur la dimension, on trouve le \emph{théorème de la dimension} qui est un résultat particulièrement important :
\begin{quote}
    Dans un espace vectoriel $E$ sur un corps $K$ de dimension finie, toutes les bases ont le même nombre d'éléments. Ce nombre
    est appelé \emph{dimension} de $E$ sur $K$ et est noté $dim_K E$.
\end{quote}
On voit bien en quoi ce résultat mérite le nom de \guillemetleft \textbf{théorème} \guillemetright : il nous donne une caractérisation de la notion de base,
ainsi qu'une méthode de catégorisation d'espaces vectoriels. Autrement dit, ce théorème nous permet de dire avec toute confiance et quiétude d'esprit
qu'une fois une base trouvée, \emph{toute} base aura le même nombre d'éléments \emph{et} que tous les espaces vectoriels dont la base contient, disons,
$n$ éléments sont reliés (ou semblables) du fait qu'ils partagent la même dimension. C'est énorme !

Mais, par définition, tout théorème doit être démontré ! Malheureusement pour nous, la preuve du \emph{théorème de la dimension} est assez longue.
Il s'agit, entre autres, de montrer qu'en remplaçant les vecteurs d'une base $\{ u_1, u_2, \ldots, u_n \}$ quelconque de $E$
par des vecteurs bien choisis d'une base $\{ v_1, v_2, \ldots, v_n \}$, on obtient toujours une base de $E$. Pour ce faire, on fait appel à plusieurs 
résultats intermédiaires, notamment au célèbre \emph{Lemme d'échange de Steinitz} \footnote{Les pointilleux d'entre vous 
remarquerons que l'on trouve rarement ce résultat sous le nom de \textit{Lemme d'échange de Steinitz} mais plutôt
sous le nom de \textit{lemme d'échange} ou \textit{lemme de Steinitz}. C'est tout à fait vrai, et il arrivera même que ceux-ci 
prennent des formes légèrement différentes. Toutefois, en général, ils font référence au même énoncé et il me semble préférable de rendre
absolument explicite le lien étroit entre ceux-ci. C'est pourquoi, malgré la possibilité d'un petit hoquet historique, 
je me permets de les combiner en un seul résultat sous le nom de lemme d'échange de Steinitz.} :

\begin{quote}
    Si $\{v_1, \ldots, v_m\}$ est une famille de vecteurs linéairement indépendants d'un espace vectoriel $E$ engendré par $\{w_1, \ldots, w_n\}$ 
    alors $m \leq n$ et, à permutation près des $w_k$, l'ensemble $\{v_1, \ldots, v_m, w_{m + 1}, \ldots, w_n\}$ engendre $E$.
\end{quote}

Ainsi, en décomposant le problème en plusieurs parties relativement simples, nous avons facilité notre tâche. C'est là toute l'utilité d'un \textbf{lemme}.

\marginnote{
    En effet, une fois ce lemme démontré, le reste de la preuve du théorème de la dimension et assez triviale. Je vous conseille de prendre quelques
    minutes pour la compléter vous-mêmes et de vérifier votre solution dans Grifone (ou bien quelque part en ligne)
}

Une fois le théorème de la dimension démontré, on arrive à une conséquence directe et presque évidente:
\begin{quote}
    \begin{enumerate}
        \item Dans un espace vectoriel de dimension n, toute famille ayant \emph{plus} de n éléments est liée.
        \item Dans un espace vectoriel de dimension n, toute famille ayant \emph{moins} de n éléments n'est pas génératrice.
    \end{enumerate}
\end{quote}
C'est un \textbf{corollaire} du théorème de la dimension.

Une fois ces résultats fondamentaux établis, on peut les étendre à d'autres résultats vus précédemment. En effet, on
peut associer le théorème de la dimension à la notion de somme pour les espace vectoriels. C'est ce qu'entreprend la proposition suivante:
\begin{quote}
    Soient $E_1, \ldots, E_p$ des espaces vectoriels de dimension finie sur le même corps $K$. Alors \[ dim_K (E_1 \times \cdots \times E_p) = dim_K E_1 + \cdots + dim_K E_p \]
\end{quote}
Ce résultat, bien qu'il ne soit pas \emph{fondamental} comme un théorème, approfondit notre compréhension de la notion de \emph{dimension}. On constate aussi
que ce résultat est moins directement lié au \emph{théorème de la dimension} puisqu'il établit une connection entre deux notions disparates. C'est à ces
caractéristiques qu'on reconnaît une \textbf{proposition}.


\begin{definition}[Conjecture]
    Une conjecture est un énoncé que l'on \emph{pense vrai mais non démontré}
\end{definition}

Fermat's last theorem (conjecture that has been proved), Euler's sum of powers conjecture (disproved), 
Riemann hypothesis (conjecture under the name of hypothesis that is yet to be proved or disproved), 
continuum hypothesis (conjecture that can be used as axiom and is therefore never needed to be proved). 

See also, Poincaré conjecture, Collatz conjecture, Goldbach conjecture. 

Conjecture in philosophy of science, Karl Popper

\begin{definition}[Preuve/Démonstration]
    Une preuve (ou une démonstration) est un enchaînement d'énoncés qui s'impliquent logiquement,
    partant d'axiomes ou d'énoncés ayant été démontrés comme étant \emph{vrais} et permettant
    de conclure que l'énoncé (à demontrer) est \emph{vrai}.
\end{definition}

Classic (painful) proofs: sqrt 2 is irrational (proof by contradiciton, can be proved constructively, see Bishop and constructivism),

\begin{kaobox}[frametitle=Commandements de la commmunication mathématique]
    \begin{enumerate}
        \item Définissez les termes
        \item Démontrez tous les énoncés
        \item Suivez les règles de la logique et de la syntaxe
        \item Écrivez pour les autres
    \end{enumerate}
\end{kaobox}

