public class Fib{
    public static int Fib(int n) {
        if (n == 0) {
            // System.out.println(0);
            return 0;
        } else if (n == 1) {
            // System.out.println(1);
            return 1;
        } else {
            // System.out.println(Fib(n-1) + Fib(n-2));
            return Fib(n-1) + Fib(n-2);
        }
    }

    public static void main(String[] args) {
        System.out.println(Fib(6));
    }
}