import java.time.chrono.ThaiBuddhistChronology;
import java.util.Scanner;

public class Processor {

    private int r0;
    private int r1;
    private int r2;
    private int r3;
    private int pc;
    private boolean C; // Carry
    private boolean N; // Negative
    private int[] mem; // memory
    private Instruction[] ins; // instruction set

    public Processor(int instructionsLength, Instruction[] ins) {
        r0 = 0;
        r1 = 0;
        r2 = 0;
        r3 = 0;
        pc = 0;
        C = false;
        N = false;
        mem = new int[32];
        ins = new Instruction[ins.length];
        for (int i = 0; i < ins.length; i++) {
            this.ins[i] = ins[i];
        }
    }

    public int fetch() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter an instruction code: ");
        int code = sc.nextInt();
        pc = (byte) (pc+1);
        return code;
    }

    public Instruction decode() {
        switch()
    }

    public void execute() {

    }

    private class InstructionSet {
    
        private int length;
        private Instruction[] ins;
        
        private InstructionSet() {
            length = 9;
            ins = new Instruction[length];
            generateInstructions();
        }
    
        public void generateInstructions() {
            ins[0] = new Instruction("LDM");
            ins[1] = new Instruction("LDI");
            ins[2] = new Instruction("STR");
            ins[3] = new Instruction("ACC");
            ins[4] = new Instruction("B");
            ins[5] = new Instruction("BC");
            ins[6] = new Instruction("BN");
            ins[7] = new Instruction("CMP");
            ins[8] = new Instruction("CLR");
        }
    
    
        private class Instruction {
        
            private String name;
            private int[] data;
        
            public Instruction(String name) {
                this.name = name;
                data = new int[8];
            }
        
        }
        
    
    
    }
    
    public static void main(String[] args) {
        
        
        Processor processor = new Processor();
        while (true) {
            int ir_1 = processor.fetch();
            int ir_2 = decode();
            execute();
        }
    }
}