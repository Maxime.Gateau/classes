#include <stdio.h>
#include <stdlib.h>

struct noeud
{
	struct noeud *gauche;
	int val;
	struct noeud *droit;
};

void insert(struct noeud **noeud, int val)
{
	if (*noeud == NULL)
	{
		*noeud=malloc(sizeof(struct noeud));
		(*noeud)->gauche=NULL;
		(*noeud)->droit=NULL;
		(*noeud)->val=val;
	}
	else
		if ((*noeud)->val < val)
			insert(&((*noeud)->gauche),val);
		else
			insert(&((*noeud)->droit),val);
}

void affiche(struct noeud *noeud)
{
	if (noeud != NULL)
	{
		affiche(noeud->droit);
		printf("  %d  ",noeud->val);
		affiche(noeud->gauche);
	}
}
	



int main()
{
	struct noeud *racine;
	racine = NULL;
	insert(&racine,3);
	insert(&racine,8);
	insert(&racine,1);
	insert(&racine,4);
	insert(&racine,13);
	insert(&racine,14);
	insert(&racine,9);
	insert(&racine,11);
	insert(&racine,6);
	insert(&racine,10);
	affiche(racine);
}
