#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>

static ucontext_t ctx[2];

void display(int val) {
    for (int i = 0; i < val; i++) {
        printf("i=%d\n",i);
        swapcontext(&ctx[1],&ctx[0]);
    }
    printf("End Display\n")
}

int main() {
    char st1[8192];
    getcontext(&ctx[1]);
    ctx[1].uc_stack.ss_sp = st1;
    ctx[1].uc_stack.ss_sp = st1;
    ctx[1].uc_link=&ctx[0];
    makecontext(&ctx[1], (void (*)(void))display,1,3);
    swapcontext(&ctx[0], &ctx[1]);
    swapcontext(&ctx[0], &ctx[1]);
    swapcontext(&ctx[0], &ctx[1]);
    swapcontext(&ctx[0], &ctx[1]);
}