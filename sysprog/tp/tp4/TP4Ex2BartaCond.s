	.data
str:	.asciz	"\n%u\n"				//ce qu'il va print en asciz
	.text
	.globl	main
main:	stmfd	sp!,{lr}			//on sauvegarde lr et on met a jour le sp
	ldr	r0,=str						//

	mov	r0,#0				//r0 = r
	mov	r1,#0x02		 		//r1 = x
	mov	r2,#0xA9				//r2 = y
	mov 	r3,#0				//paire impaire
	cmp	r1,#0
	beq	out 

while:	
	and 	r3,r1,#1			//r3 = 0
	cmp 	r3,#1 
	addeq 	r0,r2
	lsr 	r1,r1,#1
	lsl 	r2,r2,#1
	cmp	r1,#0
	bne	while
out:	mov 	r1,r0
	ldr 	r0,=str
	bl	printf
	

	
	
	mov	r0,#0
	ldmfd	sp!,{lr}
	mov	pc,lr
	//.extern printf
