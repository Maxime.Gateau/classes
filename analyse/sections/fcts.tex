\setchapterstyle{kao}
\chapter{Fonctions de plusieurs variables}

\section{Les dérivées partielles et la différentielle}

Pour une fonction $f : \R \longrightarrow \R,$ on peut définir la dérivée de $f(x)$ par rapport à $x$ comme étant la variation
de $f(x)$ lorsque $x$ change de manière infinitésimale par 
\[ \frac{d}{d x} f(x) = \lim_{h \to 0} \frac{f(x+h) - f(x)}{h}. \]

Soit une fonction $f : \R^k \longrightarrow \R^l.$ En considérant des coordonnées $\vec{x} \coloneqq (x_1, \ldots, x_k) \in \R^k,$
on écrit cela comme la donnée de vecteurs $f(x_1, \ldots, x_k) \in \R^l$ pour toute valeur $(x_1, \ldots, x_k) \in \R^k.$ Peut-on alors
donner un sens à la dérivée de $f$ ? Pour cela, on aimerait étudier la variation de $f(\vec{x})$ quand $\vec{x}$ change de manière
infinitésimale. Mais $\vec{x}$ étant mainetant un vecteur, on peut le faire varier dans différentes directions en variant différents $x_i.$
Pour se ramener au cas précédent, on peut regarder ce qu'il se passe si l'on ne varie qu'un $x_i$ à la fois, en considérant $f$ comme une 
fonction de la seule variable $x_i,$ les autres $x_j$ avec $j \neq i$ étant gardés constants. Ceci donne naissance à la notion de dérivée
partielle.

\begin{definition}[Dérivée partielle]
    Soient $\vec{x} = (x_1, \ldots, x_k) \in \R^k, U \subset \R^k$ un ouvert contenant $\vec{x}$ et $f : U \longrightarrow \R$ une fonction.
    On appelle \emph{dérivée partielle de $f$ par rapport à $x_i$ en $\vec{x}$}, si elle existe, la fonction 
    \begin{align*}
        \frac{\partial f}{\partial x_i} : \R^k &\longrightarrow \R \\
        \frac{\partial f(\vec{x})}{\partial x_i} &\coloneqq \lim_{h \to 0} \frac{f(x_1, \ldots, x_{i-1}, x_i + h, x_{i+1}, \ldots, x_k) - f(x_1, \ldots, x_{i-1}, x_i, x_{i + 1}, \ldots, x_k)}{h}.
    \end{align*}
    Si $f = (f_1, \ldots, f_l) : \R^k \longrightarrow \R^l,$ on note \[ \frac{\partial f}{\partial x_i} = \left( \frac{\partial f_1}{\partial x_i}, \ldots, \frac{\partial f_l}{\partial x_i} \right). \]
\end{definition}

En pratique, on calcule la dérivée partielle par rapport à $x_i$ en considérant toutes les autres variables comme des constantes comme dans l'exemple qui suit.

\begin{example}
    \begin{align*}
        \text{Soit } f : \R^2 &\longrightarrow \R \\
        (x,y) &\mapsto \sin(x + y + xy). \\
        \text{On a alors deux dérivées partielles données par } \\
        \frac{\partial f(x,y)}{\partial x} &= (1+y) \cos(x+y+xy) \text{ et } \\
        \frac{\partial f(x,y)}{\partial y} &= (1 + x) \cos(x + y + xy).
    \end{align*} 
\end{example}

Remarquons que la dérivée partielle par rapport à $x_i$ consiste en la dérivée de $f(\vec{x})$ lorsque $\vec{x}$ se déplace dans la direction du $i$-ème élément 
de la base canonique $e_i = (0, \ldots, 0, 1, 0, \ldots, 0)$\sidenote{le $1$ se trouvant à la $i$-ème position} : 
\[ \frac{\partial f(\vec{x})}{\partial x_i} = \lim_{h \to 0} \frac{f(\vec{x}) + h \cdot e_i - f(\vec{x})}{h} = \left[ \frac{d}{dt} f(\vec{x} + t \cdot e_i) \right]_{t=0}. \]

On peut généraliser cela en défininssant une dérivée dans n'importe quelle direction $v \in \R^k.$

\begin{definition}[Dérivée directionnelle]
    Soient $x \in \R^k,$ $v \in \R^k$ et $f : \R^k \longrightarrow \R^l.$ On appelle, si elle existe, \emph{dérivée directionnelle} la fonction
    \[ \nabla_v f(x) = \left[ \frac{d}{dt} f(\vec{x} + t \cdot v) \right]_{t=0}. \] \marginnote{$\nabla_v f(x) = \nabla f(x) \cdot v.$}
\end{definition}

Ceci généralise la notion de dérivée usuelle. Mais que veut dire \guillemetleft être dérivable \guillemetright pour ces fonctions à plusieurs variables ? On trouve un indice dans 
la notion de différetiabilité qui suit.

\begin{definition}[Différentiabilité]
    Soit $U \subset \R^n$ un ouvert. On dit qu'une application $f : U \longrightarrow \R^m$ est différentiable en $a \in U$ s'il existe une application linéaire 
    $d_a f : \R^n \longrightarrow \R^m$ telle que \[ \forall a+h \in U, \quad f(a+h) - f(a) = d_a f(h) + o(\norm{h}). \]
    $d_a f$ est appelée la différentielle de $f$ en $a.$
\end{definition}

Visuellement, la différentielle $d_a f$ est en quelque sorte la fonction obetnue en zoomant sur la fonction $f$ autour de $a.$ C'est comme voir $f$ avec un microscope centré
en $a.$ L'exemple le plus simple de différentielle est obtenu lorsque $f$ est linéaire. On a alors $f(a+h) = f(a) + f(h)$ et donc $d_a f = f.$ Considérons maintenant une 
fonction $f : \R \longrightarrow \R.$ Dans ce cas, $f$ est différentiable en $a$ \ssi  $f^{\prime}(a)$ existe et alors sa différentielle est $d_a f : h \mapsto f^{\prime}(a)+h.$
\marginnote{Notons que différentiabilité implique continuité.}

\begin{proposition}
    Soit $f : \R^k \longrightarrow \R^l.$ Si $d_a f$ existe alors $f$ est continue en $a \in \R^k.$
\end{proposition}
\begin{proof}
    Soit $h \in \R^k.$ Alors, si $d_a f$ existe, \[ f(a + h) = f(a) + d_a f(h) + o(\norm{h}). \] Puisque $d_a f$ est une application linéaire, $d_a f(h) \to 0$ quand $h \to 0.$
    Donc $f(a+h) \to f(a)$ quand $h \to 0$ et $f$ est continue en $a.$
\end{proof}

Intuitivement, la différentielle de $f$ en $a$ évaluéee selon un vecteur $v$ donne la variation de $f$ lorsque l'on s'éloigne de $a$ dans la direction $v$ comme le montre la
proposition suivante.

\begin{proposition}
    Soit $f : \R^k \longrightarrow \R^l.$ Si $d_a f$ existe, alors la dérivée $\nabla_v f(a)$ existe $\forall v \in \R^k$ et 
    \begin{align*}
        \forall v \in \R^k, &\quad d_a f(v) = \nabla_v f(a). \\
        \text{En particulier, } \qquad \forall i \in \llbracket 1, k \rrbracket, &\quad \frac{\partial f}{\partial x_i}(a) = d_a f(e_i) \\
        \text{et, pour } v = \sum_{i = 1}^{n} v_i e_i, \quad \nabla_v f(a) = \sum_{i=1}^{n} v_i \frac{\partial f}{\partial x_i}(a).
    \end{align*}
\end{proposition}
\begin{proof}
    Si $d_a f$ existe, alors \[ \forall t > 0, \forall v \in \R^k, \quad \frac{f(a+t) - f(a)}{t} = \frac{d_a f(tv) + o(t\norm{v})}{t} = d_a f(v) + \frac{o(t\norm{v})}{t}. \]
    Ceci signifie que \[ lim_{t \to 0} \frac{f(a+tv) - f(a)}{t} \coloneqq \nabla_v f(a) \text{ existe et } \nabla_v f(a) = d_a f(v). \]
    
\end{proof}
