\setchapterstyle{kao}
\chapter{Espaces métriques}

Pouvons-nous étendre la notion de convergence à des suites d'éléments d'un ensemble $X$ quelconque ? Pour $X = \mathbb{C}$ et $X$ égal à un ensemble de fonctions d'une
variable, nous avons vu que c'était possible. Mais peut-on passer à des cas a priori plus compliqués comme $X = \mathbb{C}^n$ pour $n > 1$ ou bien $X$ égal à un ensemble
de fonctions à plusieurs variables ?

\section{Métriques et normes}

Pour un ensemble $X,$ nous voulons définir la notion de distance entre deux points $x$ et $y$ de $X.$ Quelle propriété voulons-nous pour une distance ? Tout d'abord, la
distance pour aller de $x$ à $y$ devrait être la même pour aller de $y$ à $x.$ Ensuite, la distance entre deux points égaux devrait être nulle. Finalement, si l'on dispose
de trois points $x,$ $y$ et $z,$ la distance parcourue pour aller de $x$ à $y$ directement devrait être plus petite que celle parcourue en passant d'abord par $z.$ Ceci
motive la définition suivante.

\begin{definition}[Distance]
    Soit $X$ un ensemble. On appelle \emph{distance} $d$ sur $X$ toute application \[ d : X \times X \longrightarrow [0, \infty) \] vérifiant les trois propriétés suivantes.
    \begin{align*}
        \text{Symétrie : } \qquad &\forall (x,y) \in X^2, \quad && d(x,y) = d(y,x) \text{.} \\
        \text{Séparation : } \qquad &\forall (x,y) \in X^2, \quad && d(x,y) = 0 \Leftrightarrow  x = y \text{.} \\
        \text{Inégalité triangulaire : } \qquad &\forall (x,y,z) \in X^3, \quad && d(x,z) \leq d(x,y) + d(y,z) \text{.}
    \end{align*}
\end{definition}

\begin{definition}[Espace métrique]
    On appelle \emph{espace métrique} toute paire $(X,d)$ où $d$ est une distance sur l'ensemble $X.$
\end{definition}

\begin{proposition}[Sous-espace métrique]
    Soit $(X,d)$ un espace métrique. Alors pour tout sous-ensemble $Y \subset X,$ le couple $(Y, \restrict{d}{Y \times Y})$ est un espace métrique.
\end{proposition}

\marginnote{La distance euclidienne dans $\mathbb{R}^n$ est définie par \[ \forall (\vec{x},\vec{y}) \in \mathbb{R}^n \times \mathbb{R}^n, \\ 
d(\vec{x},\vec{y}) \coloneqq \sqrt{\sum_{i=1}^{n} (x_i - y_i)^2} \] où $\vec{x} \coloneqq (x_1, \ldots, x_n)$ et $\vec{y} \coloneqq (y_1, \ldots, y_n).$ Montrer que cela 
définit bien une distance est un exercice simple, mais intéressant à faire. \\ Il existe en fait de nombreuses autres distances sur $\mathbb{R}^n.$ Nous en verrons d'autres
plus loin dans le chapitre.}

\begin{proof}
    Soit $(X,d)$ un espace métrique et $Y \subset X.$ Pour tout $(x,y) \in Y^2,$ puisque $(x,y) \in X^2,$ on a que $\restrict{d}{Y \times Y}(x,y) = d(x,y)$ et les trois
    propriétés faisant de $\restrict{d}{Y \times Y}$ une distance sur $Y$ découlent automatiquement de celles de $d.$
\end{proof}

Sur $\mathbb{R}^n,$ nous avons défini la distance entre deux points $\vec{x}$ te $\vec{y}$ comme la norme du vecteur $\vec{x}-\vec(y).$ On peut en fait étendre cette notion
de norme à un ensemble quelconque.

\begin{definition}[Norme]
    Soit $V$ un K-espace vectoriel avec $K = \mathbb{R}$ ou $K = \mathbb{C}.$ On appelle \emph{norme} sur $V$ toute fonction \[ \norm{\cdot} : V \longrightarrow [0, \infty) \]
    satisfaisant les trois propriétés suivantes.
    \begin{align*}
        \text{Séparation : } \qquad &\forall v \in V, \quad && \norm{v} = 0 \Leftrightarrow v = 0 \text{.} \\
        \text{Absolue homogénéité : } \qquad &\forall \lambda \in K, \forall v \in V, \quad && \norm{\lambda \cdot v} = \abs{\lambda} \cdot \norm{v} \text{.} \\
        \text{Inégalité triangulaire : } \qquad &\forall (u,v) \in V^2, \quad && \norm{u+v} \leq \norm{u} + \norm{v} \text{.}
    \end{align*}
\end{definition}

\begin{definition}[Espace normé]
    On appelle \emph{espace normé} toute paire $(V,\norm{\cdot})$ où $\norm{\cdot}$ est une norme sur $V.$
\end{definition}

Dans le cas de $V = \mathbb{C},$ nous avons utilisé la norme définie par le module pour obtenir une distance. En fait, toute norme permet de définir une distance.

\begin{proposition}[Distance induite]
    Soit $(V, \norm{\cdot})$ un espace normé. Alors 
    \begin{align*}
        d : V \times V &\longrightarrow [0,\infty) \\
        (u,v) &\mapsto d(u,v) \coloneqq \norm{u-v}
    \end{align*}
\end{proposition}
\begin{proof}
    Vérifions les trois propriétés que doit vérifier une distance.
    \begin{enumerate}
        \item 
    \end{enumerate}
\end{proof}

\section{Fonctions continues}

Équipés de la notion de distance, nous pouvons maintenant donner un sens au fait que deux points soient proches dans un espace métrique. Ceci nous permet de définir une notion
de continuité pour une application entre deux espaces métriques.

\begin{definition}[Continuité]
    Soient $(X,d_X)$ et $(Y, d_Y)$ deux espaces métriques. On dit que qu'une application $f : X \longrightarrow Y$ est continue en $x_0 \in X$ Si
    \[ \forall \epsilon > 0, \exists \delta > 0, \forall x \in X, \quad d_X(x,x_0) < \delta \Longrightarrow d_Y( f(x), f(x_0) ) < \epsilon. \]
    On dit que $f$ est continue si elle est continue en tout point de X.
\end{definition}

Il existe de nombreuses caractérisations de la continuité d'une fonction. Pour l'une d'elle, nous allons avoir besoin d'introduire la définition suivante.

\begin{definition}[Boule ouverte]
    Soient $(X,d)$ un espace métrique, $x_0 \in X$ et $R \in (0, \infty).$ On appelle \emph{boule ouverte} de rayon $R$ centrée en $x_0$ l'ensemble
    \[ B_X(x_0, R) \coloneqq \{ x \in X : d(x, x_0) < R \}. \]
    \marginnote{Dans cette définition, le terme \og ouvert \fg se réfère au fait que le bord de la boule $d(x,x_0) = R$ n'est pas inclu dans la boule. Nous reviendrons plus tard
    sur cette notion d'ouvert de façon plus précise.}
\end{definition}

Remarquons tout de même que la notation $B_X(x_0,R)$ est un peu abusive. En effet, la boule ne dépend pas que de $X,$ mais également de la distance $d$ dont l'ensemble $X$ est
équipé. Cependant, tant que cela ne prête pas à confusion, nous l'utiliserons par la suite.

En utilisant la définition précédente, on peut réécrire la définition de la continuité de la manière suivante. $f : X \longrightarrow Y$ est continue en $x_0 \in X$ si et
seulement si \[ \forall \epsilon > 0, \exists \delta > 0, \quad f( B_X(x_0,R) ) \subset B_Y(f(x_0, \epsilon)). \]

Nous allons maintenant revisiter des propriétés classiques des fonctions sur $\mathbb{R}$ pour les étendre à ce cadre plus général.

\begin{theorem}[Composition de fonctions continues]
    Soient $(X,d_X),$ $(Y,d_Y)$ et $(Z,d_Z)$ trois espaces métriques. Si $f : X \longrightarrow Y$ et $g : Y \longrightarrow Z$ sont continues, alors 
    $g \circ f : X \longrightarrow Z$ est continue.
\end{theorem}
\begin{proof}
    
\end{proof}

\begin{proposition}
    Soit $(X,d)$ un espace métrique. Si $f : X \longrightarrow \mathbb{C}$ et $g : X \longrightarrow \mathbb{C}$ sont continues, alors $f + g$ et $f \cdot g$ sont continues 
    ainsi que $\lambda f$ pour tout $\lambda \in \mathbb{C}.$
\end{proposition}
\begin{proof}
    
\end{proof}

\subsection*{Fonctions lipschitziennes}

\begin{definition}[Fonction lipschitzienne]
    Soient $$ et $$ des espaces métriques. Une application $f : X \longrightarrow Y$ est lipschitzienne si 
    \[ \exists K > 0, \forall (x_1,x_2) \in X^2, \quad d_Y( f(x_1), f(x_2) ) \leq K \cdot d_X(x_1,x_2). \]
\end{definition}

\begin{proposition}
    Toute fonction lipschitzienne est continue.
\end{proposition}
\begin{proof}
    
\end{proof}

\section{Limite de suites}

Pour des suites de nombres ou de fonctions, nous avons pu définir la notion de convergence d'une suite. Dès que nous avons un espace métrique, nous pouvons utiliser la distance
pour exprimer le fait qu'une suite d'éléments se rapproche d'un point et ainsi définir une notion de limite.

\begin{definition}[Limite d'une suite]
    Soit $(X,d)$ un espace métrique et $(x_i)_{i \geq 0}$ une suite d'éléments de $X.$ Soit $x \in X,$ on dit que \( \lim_{n\to\infty} x_n = x \) si 
    \( \lim_{n\to\infty} d(x_n, x) = 0. \)
    En d'autres termes, on dit que \( \lim_{n\to\infty} x_n = x \) si \[ \forall \epsilon > 0, \exists N \in \mathbb{N}, \forall n \geq N, \quad d(x_n,x) < \epsilon. \]
\end{definition}

\begin{proposition}
    Soient $(X,d_X)$ et $(Y,d_Y)$ deux espaces métriques. Une application $f : X \longrightarrow Y$ est continue en $x \in X$ si et seulement si, pour toute suite 
    $(x_n) \in X^{\mathbb{N}},$ \[ \lim_{n\to\infty} x_n = x \Longrightarrow \lim_{n\to\infty} f(x_n) = f(x). \]
    La preuve de cette proposition est similaire au cas $ $ vu au semestre précédent et est laissé comme exercice.
\end{proposition}

\begin{definition}[Équivalence de norme]
    On dit que deux normes $\norm{\cdot}$ et $\norm{\cdot}'$ sont \emph{équivalentes} s'il existe $C > 0$ et $C' > 0$ tels que 
    \[ \forall v \in V, \qquad \norm{v}' \leq C\norm{v} \quad et \quad \norm{v} \leq C'\norm{v}'. \]
\end{definition}

On peut simplement vérifier qu'être équivalent définit bien une relation d'équivalence pour les normes, c'est-à-dire que l'on a les propriétés de réflexivité, de symétrie et de
transitivité. 
Si deux normes sont équivalentes, la convergence d'une suite par rapport à l'une implique la convergence par rapport à l'autre.

\begin{proposition}
    Soient $\cdots$ et $\cdots$ deux normes équivalentes sur un espace vectoriel $V$ et $(x_n) \in V^{\mathbb{N}}$ une suite. Alors $\cdots $ par rapport à $\cdots $ si et seulement si
    $\cdots $ par rapport à $\cdots .$
\end{proposition}

\dots

\section{Sous-ensembles ouverts et fermés}

\dots

\section{Espaces compacts}

\begin{definition}[Compacité]
    On dit qu'un espace métrique $(X,d)$ est \emph{compact} si pour toute suite $(x_n)$ dans X, il existe une sous-suite de $(x_n)$ convergente.
    \marginnote{La notion d'espace compact s'applique à un espace métrique alors que les notions d'ouvert et de fermé s'appliquent à des sous-espaces d'espaces métriques.}
\end{definition}

Cette définition peut être interprétée comme une extension du théorème de Bolzano-Weierstra{\ss}. En effet, ce dernier dit que l'on peut extraire une sous-suite convergente
de toute suite bornée. Ainsi, un intervalle $X = [a,b]$ est compact.
On peut alors étendre aux espaces compacts la propriété suivante déjà connue pour les cas où $X$ est un intervalle.

\begin{theorem}
    Soit $(X,d)$ un espace compact. Si $f : X \longrightarrow \mathbb{R}$ est continue, alors $\max_{x \in X} f$ et $\min_{x \in X} f$ existent. 
\end{theorem}
\begin{proof}
    
\end{proof}

\begin{theorem}[Heine-Borel]
    Pour tout $k \geq 1,$ $S \subset \mathbb{R}^k$ est compact si et seulement si $S \subset \mathbb{R}^k$ est fermé et borné.
\end{theorem}
\begin{proof}
    
\end{proof}

Ceci nous permet de construire de nombreux exemples de sous-espaces compacts dans $\mathbb{R}^k.$

\begin{theorem}
    Toutes les nomres sur $\mathbb{R}^k$ sont équivalentes.
\end{theorem}
\begin{proof}
    
\end{proof}

\section{Espaces complets}

Nous avons vu au premier semestre que toute suite de Cauchy dans $\mathbb{R}$ est convergente. Nous sommes maintenant en mesure d'étudier des suites du même type dans n'importe
quel espace métrique.

\begin{definition}[Suite de Cauchy]
    Soit $(X,d)$ un espace métrique. On dit qu'une suite $(x_n) \in X^{\mathbb{N}}$ est \emph{de Cauchy} si 
    \[ \forall \epsilon > 0, \exists n_0 \in \mathbb{N}, \forall m \geq n_0, \forall n \geq n_0. \quad d(x_n, x_m) < \epsilon. \]
\end{definition}

\begin{proposition}
    Toute suite convergente est de Cauchy.
\end{proposition}
\begin{proof}
    
\end{proof}


