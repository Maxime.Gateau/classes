#!/bin/bash

file="main.pdf"
course="algebre"

if [ -f "$course/$file" ] ; then
    rm "$file"
fi

# Compile document
pdflatex -interaction=nonstopmode -output-directory $course/out $course/main

# Compile nomenclature
# makeindex main.nlo -s nomencl.ist -o main.nls

# Compile index
# makeindex main

# Compile bibliography
# biber -output-directory out main

# Compile document
pdflatex -interaction=nonstopmode -output-directory $course/out $course/main

# Compile glossary
# makeglossaries main

# Compile document
# pdflatex -output-directory out main

# Once done, move PDF to working directory
mv $course/out/main.pdf $course/main.pdf

# If compilation is successful, push changes to GitLab
# if [ -f "$course/$file" ] ; then
#     git add *
#     read -p "Enter a commit message: " commit_msg
#     git commit -m "$commit_msg"
#     git push
# fi
