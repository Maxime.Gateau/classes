#!/bin/bash

file="main.pdf"
course="logique"

if [ -f "$course/$file" ] ; then
    rm "$course/$file"
fi

# Compile document
pdflatex -interaction=nonstopmode -output-directory $course/out $course/main.tex

# Compile nomenclature
# makeindex main.nlo -s nomencl.ist -o main.nls

# Compile index
# makeindex main

# Compile bibliography
# biber -output-directory out main

# Compile document
# pdflatex -output-directory out main

# Compile glossary
# makeglossaries main

# Compile document
# pdflatex -output-directory out main

# Once done, move PDF to working directory
mv $course/out/main.pdf $course/main.pdf
