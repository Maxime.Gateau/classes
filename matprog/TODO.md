# TODO LIST - ALGÈBRE

## Cours

- [ ] Semaine 1
- [ ] Semaine 2
- [ ] Semaine 3
- [ ] Semaine 4
- [ ] Semaine 5
- [ ] Semaine 6
- [ ] Semaine 7
- [ ] Semaine 8
- [ ] Semaine 9
- [ ] Semaine 10
- [ ] Semaine 11
- [ ] Semaine 12
- [ ] Semaine 13
- [ ] Semaine 14

## Séries / TPs

### MATLAB
- [ ] TP1
- [ ] TP2
- [ ] TP3
- [x] Test

### Maple
- [ ] TP1
- [ ] TP2
- [ ] TP3
- [ ] Test 

